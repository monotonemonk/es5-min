module es5.transforms.variables;

import es5.node;
import es5.analyse;
import es6.grammar;
import es5.utils;
import es5.transform;
import es5.eval;

version (unittest)
{
	import unit_threaded;
	import es5.testhelpers;
}

enum  identifierCharacterPenalty = ['e':1,'a':2,'t':3,'n':4,'o':5,'r':6,'c':7,'i':8,'d':9,'f':10,'s':11,'u':12,'b':13,'p':14,'l':15,'h':16,'m':17,'g':18,'k':19,'v':20,'y':21,'w':22,'j':23,'C':24,'E':25,'D':26,'I':27,'x':28,'M':29,'R':30,'S':31,'T':32,'_':33,'P':34,'O':35,'N':36,'A':37,'L':38,'q':39,'U':40,'F':41,'B':42,'V':43,'J':44,'z':45,'W':46,'H':47,'G':48,'K':49,'Y':50,'Z':51,'X':52,'Q':53,'$':54];

size_t getVariableIdentifierPenalty(string variable)
{
	size_t p = 0;
	foreach(c; variable)
	{
		p = p * 54 + identifierCharacterPenalty[c];
	}
	return p;
}
unittest {
	getVariableIdentifierPenalty("e").shouldEqual(1);
	getVariableIdentifierPenalty("ee").shouldEqual(55);
}

class BookKeepingList
{
	import std.array : Appender;
	private Appender!(Item[]) app;
	struct Item
	{
		string name, shortName;
		int usage;
	}
	void add(string name, string shortName)
	{
		app.put(Item(name,shortName));
	}
	string get(string name)
	{
		import std.range : empty, front, retro;
		import std.algorithm : find;
		auto p = app.data.retro.find!"a.name == b"(name);
		if (p.empty)
			return name;
		return p.front().shortName;
	}
	void increment(string name)
	{
		import std.range : empty, front, retro;
		import std.algorithm : countUntil;
		auto cnt = app.data.retro.countUntil!"a.name == b"(name);
		import std.stdio;
		if (cnt == -1)
			return;
		auto idx = app.data.length - (cnt + 1);
		app.data[idx].usage ++;
	}
	size_t enter()
	{
		return app.data.length;
	}
	void leave(size_t p)
	{
		app.shrinkTo(p);
	}
	void optimize(size_t since)
	{
		import std.array : array;
		import std.algorithm : sort, map;
		auto shortToLong = app.data[since..$].map!"a.shortName".array.sort!((a,b)=>a.getVariableIdentifierPenalty < b.getVariableIdentifierPenalty);
		app.data[since..$].sort!"a.usage > b.usage";
		foreach(idx, ref i; app.data[since..$])
			i.shortName = shortToLong[idx];
	}
	Item[] slice(size_t since)
	{
		return app.data[since..$].dup;
	}
	override string toString() const
	{
		import std.conv : to;
		return app.data.to!string;
	}
}

@("BookKeepingList")
unittest
{
	import unit_threaded;
	BookKeepingList list = new BookKeepingList();
	list.add("x","a");
	list.get("x").shouldEqual("a");
	auto s = list.enter();
	list.add("x","b");
	list.get("x").shouldEqual("b");
	list.leave(s);
	list.get("x").shouldEqual("a");
	s = list.enter();
	list.add("z","c");
	list.get("x").shouldEqual("a");
	list.get("z").shouldEqual("c");
	auto s2 = list.enter();
	list.add("z","a");
	list.get("z").shouldEqual("a");
	list.leave(s2);
	list.get("x").shouldEqual("a");
	list.get("z").shouldEqual("c");
	list.leave(s);
	list.get("x").shouldEqual("a");
	list.leave(0);
	list.add("first","short");
	list.add("second","verylong");
	foreach(_; 0..4)
		list.increment("second");
	list.optimize(0);
	list.get("first").shouldEqual("verylong");
	list.get("second").shouldEqual("short");
	list.add("intermezzo","bla");
	auto s3 = list.enter();
	list.add("third","mini");
	list.add("fourth","major");
	foreach(_; 0..4)
		list.increment("fourth");
	list.optimize(s3);
	list.get("intermezzo").shouldEqual("bla");
	list.get("third").shouldEqual("major");
	list.get("fourth").shouldEqual("mini");
}

@("RunOnce")
void shortenVariables(Scope scp, BookKeepingList list = new BookKeepingList())
{
	import std.array : array;
	void renameIdentifiers(Node node, BookKeepingList list)
	{
		if (node.isA!"ES6.FunctionDeclaration" || node.isA!"ES6.ArrowFunction" || node.isA!"ES6.FunctionExpression" || node.isA!"ES6.MethodDefinition" || node.isA!"ES6.FormalParameter" || node.isA!"ES6.BindingIdentifier")
			return;
		if (node.isA!"ES6.IdentifierName" && !node.parent.isA!"ES6.FieldAccessor" && !node.parent.isA!"ES6.LiteralPropertyName")
		{
			node.matches[0] = list.get(node.matches[0]);
			return;
		}
		foreach(c; node.children)
			renameIdentifiers(c,list);
	}
	void countIdentifiers(Node node, BookKeepingList list)
	{
		if (node.isA!"ES6.FunctionDeclaration" || node.isA!"ES6.ArrowFunction" || node.isA!"ES6.FunctionExpression" || node.isA!"ES6.MethodDefinition" || node.isA!"ES6.FormalParameter" || node.isA!"ES6.BindingIdentifier")
			return;
		if (node.isA!"ES6.IdentifierName" && !node.parent.isA!"ES6.FieldAccessor" && !node.parent.isA!"ES6.LiteralPropertyName")
		{
			list.increment(node.matches[0]);
			return;
		}
		foreach(c; node.children)
			countIdentifiers(c,list);
	}
	Node getIdentifierName(const Variable v)
	{
		Node node;
		if (v.node.isA!"ES6.FormalParameter") /// TODO: FormalParameter can have alternative children (BindingElement => BindingPattern)
			node = (cast(Node)v.node).getNthChild(5); /// TODO: HACKY: UGLY: casting const away
		else if (v.node.isA!"ES6.VariableDeclaration") /// TODO: Same here, can also be a BindingPattern as first child
			node = (cast(Node)v.node).getNthChild(3); /// TODO: HACKY: UGLY: casting const away
		else if (v.node.isA!"ES6.BindingIdentifier")
			node = (cast(Node)v.node).getNthChild(2); /// TODO: HACKY: UGLY: casting const away
		else
			return null;
		if (node.parent.isA!"ES6.FieldAccessor" || node.parent.isA!"ES6.LiteralPropertyName")
			return null;
		assert(node.isA!"ES6.IdentifierName");
		return node;
	}
	void countIdentifierOccuranceInChildScopes(Scope parent, string[] nonlocals, BookKeepingList list)
	{
		import std.algorithm : countUntil, remove;
		if (nonlocals.length == 0)
			return;
		void countIdentifiers(Node node, string[] nonlocals, BookKeepingList list)
		{
			if (node.isA!"ES6.FunctionDeclaration" || node.isA!"ES6.ArrowFunction" || node.isA!"ES6.FunctionExpression" || node.isA!"ES6.MethodDefinition" || node.isA!"ES6.FormalParameter" || node.isA!"ES6.BindingIdentifier")
				return;
			if (node.isA!"ES6.IdentifierName" && !node.parent.isA!"ES6.FieldAccessor" && !node.parent.isA!"ES6.LiteralPropertyName")
			{
				list.increment(node.matches[0]);
				return;
			}
			foreach(c; node.children)
				countIdentifiers(c,nonlocals,list);
		}
		foreach(c; parent.scopes)
		{
			auto current = nonlocals.dup();
			foreach(v; c.getVariables())
			{
				auto idx = current.countUntil!"a == b"(v.name);
				if (idx == -1)
					continue;
				current.remove(idx);
				current = current[0..$-1];
			}
			if (current.length == 0)
				continue;
			countIdentifiers(c.entry,current,list);
			countIdentifierOccuranceInChildScopes(c,current,list);
		}
	}
	void walkScope(Scope s, BookKeepingList list)
	{
		adjustGlobals(s,&list.get);
		auto pin = list.enter();
		scope(exit) list.leave(pin);
		foreach(v; s.getVariables)
		{
			Node node = getIdentifierName(v);
			if (node is null)
				continue;
			string id = s.getFreeIdentifier(true);
			list.add(node.matches[0],id);
		}
		countIdentifiers(s.entry,list);
		auto subList = list.slice(pin).map!"a.name".array;
		countIdentifierOccuranceInChildScopes(s,subList,list);
		list.optimize(pin);
		foreach(v; s.getVariables)
		{
			Node node = getIdentifierName(v);
			if (node is null)
				continue;
			auto id = list.get(node.matches[0]);
			node.matches[0] = id;
			s.renameVariable(v.name,id);
		}
		renameIdentifiers(s.entry,list);

		foreach(c; s.scopes)
			walkScope(c,list);
	}
	foreach (s; scp.scopes)
		walkScope(s, list);
}

unittest
{
	void assertTransformation(string js, string expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		assertTransformations!(shortenVariables)(js,expected,file,line);
	}
/*	assertTransformation(
		`var x, y, z;`,
		`var x, y, z;`
	);
	assertTransformation(
		`function bla() { var x, y, z }`,
		`function bla() { var a, c, b }`
	);
	assertTransformation(
		`function bla() { var c, b, a }`,
		`function bla() { var a, c, b }`
	);
	assertTransformation(
		`function bla(x) {}`,
		`function bla(a) {}`
	);
	assertTransformation(
		`function bla() { function w() { return 6 } }`,
		`function bla() { function a() { return 6 } }`
	);
	assertTransformation(
		`function bla() { function w() { var y; return 6 * y } }`,
		`function bla() { function a() { var a; return 6 * a } }`
	);
	assertTransformation(
		`function bla(w) { function z() { return 6 * a * w } }`,
		`function bla(b) { function c() { return 6 * a * b } }`
	);
	assertTransformation(
		`function bla(x) { function w(a) { return x * a * y * b }; var y, z }`,
		`function bla(a) { function d(d) { return a * d * c * b }; var c, e; ; }`
	);
	assertTransformation(
		`function bla(c, b, a) { function w(p) { function k(p) { return p * 2 } return a.a * b * f * e * p } function r(h, i, j) { return h * i * j * g * e * d } var f, e, d; };`,
		`function bla(f, b, c) { function h(e) { function f(a) { return a * 2 }; return c.a * b * d * a * e }; function i(b, c, d) { return b * c * d * g * a * e }; var d, a, e }; ;`
	);*/
	assertTransformation(
		`function bla() { var a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,_,$; var someLongNameThatIsUsedAllOverThePlace = 55; return function inner(multi) { someLongNameThatIsUsedAllOverThePlace * multi } };`,
		`function bla() { var p, R, S, T, _, P, O, N, A, L, q, U, B, ea, V, J, z, W, H, G, K, Y, Z, X, Q, $, F, ee, x, a, t, n, o, r, c, i, d, f, s, u, b, I, l, h, m, g, k, v, y, w, j, C, E, D, e = 55; return function M(a){ e * a } }; ;`
	);
	assertTransformation(
		`function s(o, u) { var a = c; t(function (e){ var n = t[ o ][ e ] }) }`,
		`function s(e, a) { var n = c; t(function (a){ var n = t[ e ][ a ] }) }`
	);
}