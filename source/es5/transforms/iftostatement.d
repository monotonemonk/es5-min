module es5.transforms.iftostatement;

import es5.node;
import es5.analyse;
import es6.grammar;
import es5.utils;
import es5.transform;
import es5.eval;

version (unittest)
{
	import unit_threaded;
	import es5.testhelpers;
}

bool convertIfElseAssignmentToConditionalExpression(IfStatement ifStmt)
{
	if (ifStmt.truthPath.hint & (Hint.NonValueGeneratingStatement | Hint.EmitReturn | Hint.DeducedReturingBranch))
		return false;
	if (ifStmt.elsePath.hint & (Hint.NonValueGeneratingStatement | Hint.EmitReturn | Hint.DeducedReturingBranch))
		return false;
	auto lhsA = ifStmt.truthPath.getLastStatement().getFirstAssignmentLeftHandSideExpression;
	auto lhsB = ifStmt.elsePath.getLastStatement().getFirstAssignmentLeftHandSideExpression;
	if (lhsA is null || lhsB is null)
		return false;
	auto aRef = lhsA.getNthChild(4);
	auto bRef = lhsB.getNthChild(4);

	if (!(aRef.isA!"ES6.IdentifierReference" &&
		bRef.isA!"ES6.IdentifierReference" &&
		aRef.matches[0] == bRef.matches[0]))
		return false;

	auto lhsExpr = lhsA;
	auto assignOp = lhsA.parent.children[1];
	// remove left and assignop from lhsA
	lhsA.parent.children = lhsA.parent.children[2..$];
	// remove left and assignop from lhsB
	lhsB.parent.children = lhsB.parent.children[2..$];
	// we need to convert each if's path into a AssignmentExpression
	auto condA = ifStmt.truthPath.convertToAssignmentExpression();
	auto condB = ifStmt.elsePath.convertToAssignmentExpression();
	auto expr = ifStmt.condition.children[0];
	auto rhsExpr = expr.children[$-1].children[0].children[0];
	assert(rhsExpr.isA!"ES6.RightHandSideExpression");
							// combine RHSExpr, assignA, assignB into ConditionalExpression and replace nodes
	auto condExpr = new Node(ParseTree("ES6.ConditionalExpression",true),[rhsExpr,condA,condB]);
	auto assignExpr = new Node(ParseTree("ES6.AssignmentExpression",true),[lhsExpr,assignOp,condExpr]);
	auto stmt = ifStmt.parent;
	ifStmt.truthPath.branch.removeFromParent();
	ifStmt.elsePath.branch.removeFromParent();
	stmt.setChild(0,assignExpr.createExpressionStatementFromAssignmentExpression);
	if (expr.children.length > 1)
	{
		// if the expression had more AssignmentExpression seperated by comma's, dump them before this one.
		stmt.getNthChild(2).insertAtFront(expr.children[0..$-1]);
	}
	reanalyse(stmt);
	return true;
}

bool convertIfsToExpressionStatementsOrConditionals(Node node)
{
	auto ifStmt = node.asIfStatement();
	// when an if statement is the only child of a statementlist in another if statement
	if (ifStmt.isNull)
		return false;
	if (!ifStmt.hasElsePath)
	{
		if (!(ifStmt.truthPath.hint & (Hint.NonValueGeneratingStatement | Hint.EmitReturn)))
		{
			auto condExpr = ifStmt.getNthChild(2);
			auto b = ifStmt.truthPath.branch;
			auto holder = node.parent;
			auto truthPath = ifStmt.truthPath;
			if (truthPath.isBlockStatement)
			{
				import std.algorithm : map, all;
				import std.range : array;
				auto list = truthPath.getStatementList();
				if (list is null)
					return false;
				auto exprStatements = list.children.map!(c=>c.getNthChild(2)).array();
				if (exprStatements.length > 0 && exprStatements.all!(c=>c.isA!"ES6.ExpressionStatement"))
				{
					b.parent.removeChild(b);
					auto exprIn = exprStatements[0].children[0];
					foreach(c; exprStatements[1..$])
					{
						exprIn.insertAtBack([new Node(ParseTree("ES6.Comma",true,[","])),c.getNthChild(2)]);
					}
					combineConditionalExpressions(condExpr.asExpression,exprIn.asExpression);
					auto exprStmt = new Node(ParseTree("ES6.ExpressionStatement",true),[node.getNthChild(2)]);
					holder.setChild(0,exprStmt);
					reanalyse(exprStmt);
					return true;
				}
			} else if (truthPath.isExpressionStatement)
			{
				b.parent.removeChild(b);
				auto expr = truthPath.getExpression();
				combineConditionalExpressions(condExpr.asExpression,expr.asExpression);
				auto exprStmt = new Node(ParseTree("ES6.ExpressionStatement",true),[node.getNthChild(2)]);
				holder.setChild(0,exprStmt);
				reanalyse(exprStmt);
				return true;
			}
		}
	} else // has else path
	{
		if (convertIfElseAssignmentToConditionalExpression(ifStmt))
			return true;
		auto truthPath = ifStmt.truthPath;
		auto elsePath = ifStmt.elsePath;

		if (truthPath.hint & Hint.NonValueGeneratingStatement)
			return false;
		if (elsePath.hint & Hint.NonValueGeneratingStatement)
			return false;
		if (truthPath.isEmpty() || elsePath.isEmpty())
			return false;
		Node truthAssignExpr, elseAssignExpr, rhsExpression;
		if (ifStmt.doBothBranchesAlwaysReturn())
		{
			if (truthPath.isBlockStatement)
				truthAssignExpr = truthPath.getStatementList().convertToAssignmentExpression();
			else
				truthAssignExpr = truthPath.convertToAssignmentExpression();
			if (elsePath.isBlockStatement)
				elseAssignExpr = elsePath.getStatementList().convertToAssignmentExpression();
			else
				elseAssignExpr = elsePath.convertToAssignmentExpression();

			auto expr = ifStmt.condition().children[0];
			auto condExpr = expr.children[$-1].children[$-1];
			condExpr.addChildren([truthAssignExpr,elseAssignExpr]);
			auto returnStmt = new Node(ParseTree("ES6.ReturnStatement",true),[expr]);
			node.parent.setChild(0,returnStmt);
			returnStmt.propagateBranch(node.branch);
			reanalyse(condExpr);
			return true;
		}
		if (truthPath.returns() || elsePath.returns())
			return false;
		truthPath.branch.removeFromParent();
		elsePath.branch.removeFromParent();
		// do `c ? a : b`
		if (truthPath.isBlockStatement)
			truthAssignExpr = truthPath.getStatementList().convertToAssignmentExpression();
		else
			truthAssignExpr = truthPath.convertToAssignmentExpression();
		if (elsePath.isBlockStatement)
			elseAssignExpr = elsePath.getStatementList().convertToAssignmentExpression();
		else
			elseAssignExpr = elsePath.convertToAssignmentExpression();

		assert(truthAssignExpr.isA!"ES6.AssignmentExpression");
		assert(elseAssignExpr.isA!"ES6.AssignmentExpression");

		auto expr = ifStmt.condition().children[0];
		auto lastAssignmentExpression = expr.children[$-1];
		Node condExpr;
		if (lastAssignmentExpression.hint & Hint.Assignment)
		{
			condExpr = lastAssignmentExpression.parenthesisAssignmentExpressionIntoConditionalExpression();
			expr.setChildReverse(0,new Node(ParseTree("ES6.AssignmentExpressionIn",true),[condExpr]));
		} else
		{
			condExpr = expr.children[$-1].children[$-1];
		}

		condExpr.addChildren([truthAssignExpr,elseAssignExpr]);
		auto exprStmt = new Node(ParseTree("ES6.ExpressionStatement",true),[expr]);
		node.parent.setChild(0,exprStmt);
		exprStmt.propagateBranch(node.branch);
		reanalyse(condExpr);
		return true;
	}
	return false;
}

@("Convert Ifs to Expression Statements or Conditionals")
unittest
{
	void assertTransformation(string js, string expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		assertTransformations!(convertIfsToExpressionStatementsOrConditionals)(js,expected,file,line);
	}
	assertTransformation(
		`if (doFun(), a) { b = 6; } else { b = 7; }`,
		`doFun(),b = a ? 6 : 7`
	);
	/// Issue #89
	assertTransformation(
		`if (def = fun()) { a = 7*def; } else { b = 99*def; }`,
		`(def = fun()) ? a = 7 * def : b = 99 * def`
	);
	/// Issue #86
	assertTransformation(
		`if (a) b = 6; else { for(;;)break; b = 7 }`,
		`if (a) b = 6; else { for(;;)break; b = 7 }`
	);
	/// ditto
	assertTransformation(
		`function doo() { if (a) b = 6; else { if (d) return 7; b = 7; } }`,
		`function doo() { if (a) b = 6; else { if (d) return 7; b = 7 } }`
	);
	/// Issue #85
	assertTransformation(
		`function bla() { if (b) { if (a) { return a } else { return } } }`,
		`function bla() { if (b) { return a ? a : void 0 } }`
	);
	/// ditto
	assertTransformation(
		`function bla() { if (b) { if (a) return a; else return; } }`,
		`function bla() { if (b) { return a ? a : void 0 } }`
	);
	/// ifs without elses get transformed into `condition && truthpath`
	assertTransformation(
		`if (a) doBla()`,
		`a && doBla()`
	);
	/// if else with returns get a return in front and a ternary if statement `a ? b : c`
	assertTransformation(
		`function a(b) { if (b.op) { return cd(b) } else if (b.kl) { return cf(b) }; else return b.kk }`,
		`function a(b) { return b.op ? cd(b) : b.kl ? cf(b) : b.kk }`
	);
	/// ditto
	assertTransformation(
		`function a(b) { if (b.op) { return cd(b) } else { k=6; if (b.kl) { f = 7; return cf(b) }; else { p = 5; return b.kk } } }`,
		`function a(b) { return b.op ? cd(b) : (k = 6,b.kl ? (f = 7,cf(b)) : (p = 5,b.kk)) }`
	);
	/// if else without returns we convert into ternary if statement `a ? b : c`
	assertTransformation(
		`if (bla) doB() else { goP() }`,
		`bla ? doB() : goP()`
	);
	/// ditto
	assertTransformation(
		`if (a) { e(); g = 6; } else { f(); k = 7; }`,
		"a ? (e(),g = 6) : (f(),k = 7)"
	);
	/// if else assignments to same identifier get transformed into `a = b ? c : d`
	assertTransformation(
		`if (a) g = 6; else k = 7;`,
		"a ? g = 6 : k = 7"
	);
	/// ditto
	assertTransformation(
		`if (a) { g = 6; } else { k = 7; }`,
		"a ? g = 6 : k = 7"
	);
	/// ditto
	assertTransformation(
		`if (a) b = 6; else b = 7;`,
		`b = a ? 6 : 7`
	);
	/// ditto
	assertTransformation(
		`if (a) { b = 6; } else { b = 7; }`,
		`b = a ? 6 : 7`
	);
	/// ditto
	assertTransformation(
		`if (a) b = 6; else { b = 7; }`,
		`b = a ? 6 : 7`
	);
	/// ditto
	assertTransformation(
		`if (a) { b = 6; } else b = 7;`,
		`b = a ? 6 : 7`
	);
	/// ditto
	assertTransformation(
		`if (a) { d = 5; b = 6; } else {g = 12; b = 7};`,
		`b = a ? (d = 5,6) : (g = 12,7)`
	);
	/// ditto
	assertTransformation(
		`if (a) b = 7; else b = 8; if (c) d = 6; else d = 9;`,
		`b = a ? 7 : 8; d = c ? 6 : 9`
	);
	/// we don't transform empty statements
	assertTransformation(
		`if (bla) {  } else { doHup() }`,
		`if (bla) {  } else { doHup() }`
	);
	/// ditto
	assertTransformation(
		`if (bla) { doHup() } else {  }`,
		`if (bla) { doHup() } else {  }`
	);
	/// ditto
	assertTransformation(
		`if (bla) ; else { doHup() }`,
		`if (bla) ; else { doHup() }`
	);
	/// ditto
	assertTransformation(
		`if (bla) { doHup() } else  ;`,
		`if (bla) { doHup() } else  ;`
	);
	/// if only one branch returns we don't transform
	assertTransformation(
		`function abc() { if (bla) { return 7 } else { deHup() } }`,
		`function abc() { if (bla) { return 7 } else { deHup() } }`
	);
	/// ditto
	assertTransformation(
		`function abc() { if (bla) { doHup() } else { return 7 } }`,
		`function abc() { if (bla) { doHup() } else { return 7 } }`
	);
	/// Issue #101
	assertTransformation(
		`if(y) b=2; else if(k) b=5; else b=7;`,
		`b = y ? 2 : k ? 5 : 7`
	);
}