module es5.transforms.expressions;

import es5.node;
import es5.analyse;
import es6.grammar;
import es5.utils;
import es5.transform;
import es5.eval;

bool simplifyRedundantAssignmentExpressions(Node node)
{
	auto expr = node.asExpression;
	if (expr.isNull || expr.isSingleExpression)
		return false;
	bool isAssignment = expr.children[$-3].children.length > 1;
	if (!isAssignment)
		return false;

	if (expr.children[$-1].getNthChild(1).children.length > 1)
		return false;

	auto lhsA = expr.children[$-3].children[0].asLHSExpression();
	auto lhsB = expr.children[$-1].getNthChild(5).asLHSExpression();
	
	assert(!lhsA.isNull);
	if (lhsB.isNull)
		return false;
	if (lhsA.hasArguments() || lhsB.hasArguments())
		return false;
	if (!lhsA.isPrimaryExpression() || !lhsB.isPrimaryExpression())
		return false;

	auto primA = lhsA.getPrimaryExpression();
	auto primB = lhsB.getPrimaryExpression();

	if (!primA.children[0].isA!"ES6.IdentifierReference" ||
		!primB.children[0].isA!"ES6.IdentifierReference")
		return false;
	if (primA.children[0].matches != primB.children[0].matches)
		return false;

	auto accA = lhsA.getMemberExpressionAccessors();
	auto accB = lhsB.getMemberExpressionAccessors();

	if (accA.length != accB.length)
		return false;
	import std.range : zip;
	foreach(c; accA.zip(accB))
	{
		if (!sameTree(c[0],c[1]))
			return false;
	}
	node.children = node.children[0..$-2];
	return true;
}

@("simplifyRedundantAssignmentExpressions")
unittest
{
	import es5.testhelpers;
	import unit_threaded;
	void assertTransformation(string js, string expected)
	{
		assertTransformations!(simplifyRedundantAssignmentExpressions)(js,expected);
	}
	assertTransformation
	(
		`if (a = 5, a) doBla();`,
		`if (a = 5) doBla();`
	);
	assertTransformation
	(
		`if (a.bla = 5, a.bla) doBla();`,
		`if (a.bla = 5) doBla();`
	);
	assertTransformation
	(
		`if (a[4] = 5, a[4]) doBla();`,
		`if (a[ 4 ] = 5) doBla();`
	);
	assertTransformation
	(
		`if (a.bla[4].tar = 5, a.bla[4].tar) doBla();`,
		`if (a.bla[ 4 ].tar = 5) doBla();`
	);

	assertTransformation
	(
		`if (a = 5, b) doBla();`,
		`if (a = 5,b) doBla();`
	);
	assertTransformation
	(
		`if (a = 5, a.bla) doBla();`,
		`if (a = 5,a.bla) doBla();`
	);
	assertTransformation
	(
		`if (a.bla = 5, a.ops) doBla();`,
		`if (a.bla = 5,a.ops) doBla();`
	);
	assertTransformation
	(
		`if (a[4] = 5, a[5]) doBla();`,
		`if (a[ 4 ] = 5,a[ 5 ]) doBla();`
	);

	assertTransformation
	(
		`function a() { return b = 7, b }`,
		`function a() { return b = 7 }`
	);
	assertTransformation
	(
		`function a() { return b = 7, b ? ab() : ko() }`,
		`function a() { return b = 7,b ? ab() : ko() }`
	);
	assertTransformation
	(
		`if (a.bla() = 4,a) ;`,
		`if (a.bla() = 4,a) ;`
	);
	assertTransformation
	(
		`if (a = 4,a()) ;`,
		`if (a = 4,a()) ;`
	);
	assertTransformation
	(
		`if (super.a = 4,a) ;`,
		`if (a = 4,super.a) ;`
	).shouldThrow();	// (we don't support super yet in tojs, however we want to include the branch for coverage)
	assertTransformation
	(
		`if (123 = 4,123) ;`,
		`if (123 = 4,123) ;`
	);
}