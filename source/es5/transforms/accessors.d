module es5.transforms.accessors;

import es5.node;
import es5.analyse;
import es6.grammar;
import es5.utils;
import es5.transform;
import es5.eval;

@("RunOnce")
void shortenOftenUsedAccessors(Scope scp)
{
	import std.conv : to;
	import std.array : array;
	struct Stats
	{
		int count;
		ulong length;
		Scope scp;
		size_t depth;
		Node[] literals;
	}
	foreach(s; scp.scopes)
	{
		Node node = s.entry;
		Stats[string] dict;
		void walk(Node node)
		{
			if (node.isA!"ES6.FieldAccessor" && (node.parent.isA!"ES6.MemberExpression" || node.parent.isA!"ES6.CallExpression"))
			{
				auto str = node.children[0].matches[0];
				if (auto p = str in dict)
				{
					if (node.scp !is (*p).scp)
					{
						auto commonScp = findCommonParent((*p).scp,node.scp);
						(*p).scp = commonScp;
						(*p).depth = getScopeDepth(commonScp);
					}
					(*p).literals ~= node;
					(*p).count++;
				}
				else
					dict[str] = Stats(1,str.length,node.scp,getScopeDepth(node.scp),[node]);
			}
			foreach (c; node.children)
				walk(c);
		}
		walk(node);
		foreach (string str, ref Stats stats; dict)
		{
			if (stats.count == 1)
				continue;
			auto len = str.length + 1;
			if (cast(int)(3+len+stats.count+1) >= cast(int)(len*stats.count))
				continue;
			string id = getFreeIdentifierForAllScopesInScope(stats.scp, stats.literals.map!("a.scp"));
			int charactersSaved = cast(int)(len*stats.count) - cast(int)(3+len+(id.length*(stats.count+1)));
			if (charactersSaved <= 0)
				continue;
			assert(stats.scp.parent !is null);
			auto list = stats.scp.entry.getChild("ES6.StatementList");
			auto vDeclList = list.getVariableDeclarationList;
			auto decl = createVariableDeclarationNode(id);
			if (vDeclList is null || !vDeclList.isA!"ES6.VariableDeclarationList")
			{
				decl.addChild(createStringLiteralInitializer(str));
				auto item = createVariableDeclarationListStatement([decl]);
				list.insertAtFront(item);
				propagateBranch(item,list.branch);
				reanalyse(decl);
			} else
			{
				decl.addChild(createStringLiteralInitializer(str));
				vDeclList.insertAtFront(decl);
				propagateBranch(decl,vDeclList.branch);
			}
			assert(!stats.scp.hasVariable(id));
			stats.scp.addVariable(Variable(id,IdentifierType.Variable,decl));
			foreach(lit; stats.literals)
			{
				auto idx = lit.parent.findIndex(lit);
				auto acc = createSquareBracketAccessor(id);
				lit.parent.setChild(idx,acc);
				propagateBranch(acc,lit.branch);
				list.scp.addIdentifier(id);
			}
		}
	}
}

unittest
{
	import es5.testhelpers;
	void assertTransformation(string js, string expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		assertTransformations!(shortenOftenUsedAccessors)(js,expected,file,line);
	}
	assertTransformation(
		`function bla() { var a = {object}; e = a.object; a.object = e; doBla(a.object); a.object = doBla(); }`,
		`function bla() { var t = "object", a = { object }; e = a[ t ]; a[ t ] = e; doBla(a[ t ]); a[ t ] = doBla() }`
	);
	assertTransformation(
		`function bla() { function cd() { a = {object}; e = a.object; a.object = e; } function de() { doBla(a.object); a.object = doBla(); } }`,
		`function bla() { var t = "object"; function cd() { a = { object }; e = a[ t ]; a[ t ] = e }; function de() { doBla(a[ t ]); a[ t ] = doBla() } }`
	);
}