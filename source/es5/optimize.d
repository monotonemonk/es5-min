module es5.optimize;

import es5.node;
import es5.analyse;
import es6.grammar;
import es5.utils;
import es5.transform;
import es5.eval;
import es5.transforms.expressions;
import es5.transforms.iftostatement;
import es5.transforms.parenthesis;
import es5.transforms.switches;
import es5.transforms.accessors;
import es5.transforms.voidparenthesis;
import es5.transforms.loops;

struct DepthFirst
{

}

void convertToBase10Decimals(Node node)
{
	if (node.isA!"ES6.HexIntegerLiteral")
	{
		import std.conv : parse, to;
		string hex = node.matches[0][2..$];
		int dec = hex.parse!int(cast(uint)16);
		node.parent.matches[0] = dec.to!string;
		node.parent.children.length = 0;
	}
	foreach(c; node.children)
		convertToBase10Decimals(c);
}
bool shortCircuitIfStatements(Node node, bool doReanalyse = false)
{
	if (node.isA!"ES6.ConditionalExpression" && node.children.length > 1)
	{
		auto orgAssignExpr = node.parent;
		auto idx = orgAssignExpr.parent.findIndex(orgAssignExpr);
		auto rightHandExpr = node.children[0];
		auto trueAssignExpr = node.children[1];
		auto falseAssignExpr = node.children[2];
		RawValue raw;
		if (rightHandExpr.children[0].isA!"ES6.BinaryExpression")
			raw = rightHandExpr.children[0].getRawValue!"ES6.BinaryExpression";
		else
			raw = rightHandExpr.children[0].getRawValue!"ES6.UnaryExpression";
		if (raw.type != ValueType.NotKnownAtCompileTime)
		{
			auto val = raw.coerceAsBoolean;
			Node assignToKeep;
			if (val)
				assignToKeep = trueAssignExpr;
			else
				assignToKeep = falseAssignExpr;
			orgAssignExpr.parent.children[idx] = assignToKeep;
			assignToKeep.parent = orgAssignExpr.parent;
			return true;
		}
	} else if (node.isA!"ES6.IfStatement")
	{
		auto ifStmt = node.asIfStatement;
		auto expr = ifStmt.conditionExpression();
		RawValue raw;
		if (expr.isSingleExpression())
			raw = expr.getRawValue!"ES6.Expression";
		else
			raw = expr.getLastAssignmentExpression().getRawValue!"ES6.AssignmentExpression";

		if (raw.type != ValueType.NotKnownAtCompileTime)
		{
			auto val = raw.coerceAsBoolean;
			IfPath pathToKeep;
			auto assignmentSiblings = expr.children.length == 1 ? [] : expr.children[0..$-2];
			if (val)
				pathToKeep = ifStmt.truthPath;
			else
			{
				if (node.children.length == 2)
				{
					if (assignmentSiblings.length > 0)
						node.parent.setChild(0, createExpressionStatementFromCommaSeparatedAssignments(assignmentSiblings));
					else
						node.parent.setChild(0,new Node(ParseTree("ES6.EmptyStatement",true,[";"])));
					return true;
				}
				pathToKeep = ifStmt.elsePath;
			}
			if (pathToKeep.isBlockStatement)
			{
				auto item = node.parent.parent;
				if (!item.isA!"ES6.StatementListItem")
				{
					auto block = convertStatementIntoBlockStatement(node.parent);
					//block.propagateBranch(node.branch);
					item = node.parent.parent;
				} /*else
					pathToKeep.getNthChild(4).children.propagateBranch(node.branch);*/
				auto list = item.parent.asStatementList();
				auto idx = list.findIndex(item);
				list.remove(idx);
				list.insertInPlace(idx,pathToKeep.getStatementList.children);
				if (assignmentSiblings.length > 0)
					list.insertInPlace(idx,[createStatementListItemFromCommaSeparatedAssignments(assignmentSiblings)]);
			} else
			{
				if (assignmentSiblings.length > 0)
				{
					auto item = node.parent.parent;
					if (!item.isA!"ES6.StatementListItem")
					{
						auto block = convertStatementIntoBlockStatement(node.parent);
						//block.propagateBranch(node.branch);
						item = node.parent.parent;
					} /*else
						pathToKeep.getNthChild(4).children.propagateBranch(node.branch);*/
					auto list = item.parent.asStatementList();
					auto idx = list.findIndex(item);
					list.insertInPlace(idx,[createStatementListItemFromCommaSeparatedAssignments(assignmentSiblings)]);
					// do something special
				}
				node.parent.setChild(0,pathToKeep.children[0]);
			}
			return true;
		}
	}
	return false;
}
bool reduceBinaryExpressions(Node node)
{
	if (node.isA!"ES6.BinaryExpression" && node.children.length > 1)
	{
		auto raw = node.getRawValue!"ES6.BinaryExpression";
		if (raw.type != ValueType.NotKnownAtCompileTime)
		{
			auto expr = raw.toUnaryExpression();
			node.parent.setChild(0,expr);
			return true;
		}
	}
	return false;
}
bool removeEmptyStatementsFromStatementLists(Node node)
{
	if (node.isA!"ES6.StatementList" || node.isA!"ES6.ModuleItemList")
	{
		bool r = false;
		for (size_t i = 0, l = node.children.length; i < l;)
		{
			auto c = node.children[i];
			if (c.getNthChild(2).isA!"ES6.EmptyStatement")
			{
				node.removeChild(i);
				--l;
				r = true;
			}
			else
				++i;
		}
		return r;
	}
	return false;
}
// NOTE: Can be replaced with `import std.traits : Parameters;` when gdc/ldc support it
import std.traits : isCallable, FunctionTypeOf;
template Parameters(func...)
	if (func.length == 1 && isCallable!func)
{
	static if (is(FunctionTypeOf!func P == function))
		alias Parameters = P;
	else
		static assert(0, "argument has no parameters");
}
void runOptimizations(fun...)(Node node, in string file = __FILE__, in size_t line = __LINE__)
{
	import std.typetuple : staticMap;
	import std.functional : unaryFun;
	import std.traits : ReturnType, isBoolean, hasUDA;

	alias _funs = staticMap!(unaryFun, fun);

	foreach(_fun; _funs)
	{
		static if (!isBoolean!(ReturnType!(_fun)) && hasUDA!(_fun,"RunOnce"))
		{
			static if (is(Parameters!_fun[0] : Node))
			{
				_fun(node);
			} else static if (is(Parameters!_fun[0] : Scope))
			{
				_fun(node.scp);
			}
		}
	}

	Node[] todo = [node];
	bool runNodes(Node node, bool entry = true)
	{
		bool r = false;
		foreach(c; node.children.dup)
		{
			if (c.isA!"ES6.FunctionStatementList")
				todo ~= c;
			else
				r |= runNodes(c,false);
		}
		version(unittest)
		{
			import unit_threaded;
			import es5.tojs;
			import std.traits : fullyQualifiedName;
		}
		foreach(_fun; _funs)
		{
			static if (isBoolean!(ReturnType!(_fun)))
			{
				static if (is(Parameters!_fun[0] : Node))
				{
					if (_fun(node))
					{
						/*version(unittest)
						{
							writelnUt("Did ",fullyQualifiedName!_fun);
							writelnUt(node.getRoot().toJS);
						}
						version(unittest)
						{
							assertTree(node,fullyQualifiedName!_fun,file,line);
						}*/
						r |= true;
					}
				} else static if (is(Parameters!_fun[0] : Scope))
				{
					if (entry && _fun(node.scp))
					{
						/*version(unittest)
						{
							writelnUt("Did ",fullyQualifiedName!_fun);
							writelnUt(node.getRoot().toJS);
						}
						version(unittest)
						{
							assertTree(node,fullyQualifiedName!_fun,file,line);
						}*/
						r |= true;
					}
				}
			} else static if (!hasUDA!(_fun,"RunOnce"))
			{
				static if (is(Parameters!_fun[0] : Node))
				{
					_fun(node);
				} else static if (is(Parameters!_fun[0] : Scope))
				{
					if (entry)
						_fun(node.scp);
				}
			}
		}
		return r;
	}
	for (auto i = 0; i < todo.length; i++)
		while(runNodes(todo[i]))
		{
		}
}
// Note: these are optimizations we can run without having AnalysisResult (scope, branches and hints) and are run before variables and function declarations are moved
auto optimize(Node node)
{
	convertToBase10Decimals(node);
	version(Chatty)
	{
		import std.stdio : writeln, write;
		import es5.tojs;
		writeln(node.toJS);
	}
	runOptimizations!(
		shortCircuitIfStatements,
		reduceBinaryExpressions,
		removeEmptyStatementsFromStatementLists
	)(node);
	version(Chatty)
	{
		import std.stdio : writeln, write;
		import es5.tojs;
		writeln(node.toJS);
	}
}
auto optimize(Node root, AnalysisResult ar)
{
	bool optimizeReturningIfStatement(Scope s)
	{
		import std.algorithm : remove;
		assert(s.entry);
		assert(s.entry.branch);
		// cycle through scope, if there is a branch that returns (undefined/empty)
		foreach (branch; s.entry.branch.children)
		{
			Hints hint = Hint.ReturningBranch | Hint.EmptyReturn;
			if ((branch.entry.hint & hint) != hint)
				continue;
			auto stmt = branch.entry.parent;
			if (!stmt.isA!"ES6.IfStatement")
				continue;
			auto ifStmt = stmt.asIfStatement();
			if ((ifStmt.truthPath.hint & hint) != hint)
				continue;

			auto statement = branch.entry.parent.parent.parent;
			auto idx = statement.parent.findIndex(statement);
			auto transfers = statement.parent.children[idx+1..$];
			if (ifStmt.truthPath.isBlockStatement)
			{
				auto list = ifStmt.truthPath.getStatementList;

				if (list.children.length > 1)
				{
					// remove return statement from if true path
					list.keepInStatementListUntilSubStatement!"ES6.ReturnStatement";
					// create else path
					if (transfers.length > 0)
					{
						if (ifStmt.children.length == 4)
						{
							// there is already an else block
							auto elseBranch = ifStmt.elsePath.branch;
							Node elseList;
							if (!ifStmt.elsePath.isBlockStatement)
							{
								elseList = ifStmt.elsePath.convertToBlockStatement().getStatementList();
							} else
								elseList = ifStmt.elsePath.getStatementList();
							elseList.addChildren(transfers);
							ifStmt.elsePath.propagateBranch(elseBranch);
							statement.parent.children.length = idx+1;
							reanalyse(elseList);
							return true;
						} else
						{
							// TODO: This can be replaced with the createElsePath function, as used in putRemainingStatementsInElseBlockWhenIfReturns below
							auto elseKeyword = new Node(ParseTree("ES6.ElseKeyword",true,["else"]));
							auto ffp = new Node(ParseTree("ES6.StatementListReturn",true),transfers);
							auto elsePath = new Node(ParseTree("ES6.StatementReturn",true,["{"]),
								[
									new Node(ParseTree("ES6.BlockStatementReturn",true),
									[
										new Node(ParseTree("ES6.CurlyBrackets",true),
										[
											ffp
										])
									])
								],ifStmt);
							elsePath.scp = branch.entry.scp;
							// create a new branch for the else path and move all branches after the if inside it
							auto elseBranch = new Branch(statement.parent.branch,elsePath);
							size_t idx2 = findIndex(ifStmt.branch,ifStmt.truthPath.branch);
							elseBranch.children = ifStmt.branch.children[idx2+1..$];
							ifStmt.branch.children = ifStmt.branch.children[0..idx2+1];
							ifStmt.branch.children ~= elseBranch;
							elsePath.propagateBranch(elseBranch); // correct all branch pointers

							// insert the new nodes
							ifStmt.addChild(elseKeyword);
							ifStmt.addChild(elsePath);

							// cut out all statements that are now inside the else block
							statement.parent.children.length = idx+1;
							reanalyse(ffp);
							return true;
						}
					}
					continue;
				} else if (!list.children[0].getNthChild(2).isA!"ES6.ReturnStatement")
					continue;
			} else if (!ifStmt.truthPath.children[0].isA!"ES6.ReturnStatement")
				continue;
			if (ifStmt.hasElsePath)
			{
				if (transfers.length > 0)
				{
					// move statements into else block and force else block to be a BlockStatment
					auto elseBranch = ifStmt.elsePath.branch;
					if (!ifStmt.elsePath.isBlockStatement)
						ifStmt.elsePath = ifStmt.elsePath.convertToBlockStatement;
					auto list = ifStmt.elsePath.getStatementList;
					list.addChildren(transfers);
					ifStmt.elsePath.propagateBranch(elseBranch);
					statement.parent.children.length = idx+1;
					reanalyse(list);
				}
				// remove truth path, negate if, move else block to truth path
				auto bidx = branch.parent.findIndex(branch);
				branch.entry.parent.negateIfCondition();
				branch.parent.children.remove(bidx);
				ifStmt.truthPath = ifStmt.elsePath;
				ifStmt.children.length = 2;
				reanalyse(ifStmt);
				return true;
			}
			if (transfers.length == 0)
			{
				if (ifStmt.truthPath.isBlockStatement)
				{
					auto list = ifStmt.truthPath.getStatementList();
					list.keepInStatementListUntilSubStatement!"ES6.ReturnStatement";
					continue;
				}
				auto empty = new Node(ParseTree("ES6.EmptyStatement",true,[";"]));
				ifStmt.truthPath.children[0] = empty;
				empty.parent = ifStmt.truthPath;
				propagateBranch(empty,ifStmt.truthPath.branch);
				continue;
			}
			// negate the condition
			ifStmt.negateIfCondition();
			ifStmt.truthPath.setListChildren(transfers);
			statement.parent.children.length = idx+1;
			reanalyse(ifStmt.truthPath.getStatementList);
			return true;
		}
		return false;
	}
	bool removeStatementBlockFromSwitchCaseClauses(Node node)
	{
		if (!node.isA!"ES6.CaseClause" || node.children.length == 1)
			return false;
		auto statementList = node.children[1].asStatementList();
		if (statementList.isNull)
			return false;
		Node[] nodes;
		bool didSomething = false;
		foreach (s; statementList.children)
		{
			auto bs = s.getNthChild(2);
			if (bs !is null && bs.isA!"ES6.BlockStatement")
			{
				foreach (child; bs.getStatementListFromBlockStatement().children)
				{
					nodes ~= child;
					didSomething = true;
				}
			} else
				nodes ~= s;
		}
		statementList.setChildren(nodes);
		return didSomething;
	}
	void compressReturningSwitch()
	{
		// when a switch has all paths return something, we can rewrite the whole thing into a if(...) return if(...) return
	}
	bool putRemainingStatementsInElseBlockWhenIfReturns(Node node)
	{
		auto ifStmt = node.asIfStatement();
		// when we encounter an if-statement where the first branch returns
		if (!ifStmt.isNull && ifStmt.truthPath.hint & Hint.ReturningBranch && ifStmt.truthPath.hint & Hint.Return)
		{
			auto statement = node.parent.parent;
			auto idx = statement.parent.findIndex(statement);
			auto transfers = statement.parent.children[idx+1 .. $];
			// and there are statements after the if-statement (not counting the else block)
			import std.algorithm : any;
			if (transfers.length > 0 && transfers.any!(t=>(t.hint & Hint.ReturningBranch || t.hint & Hint.Return || t.hint & Hint.EmitReturn)))
			{
				// and the if statement doesn't have an else block
				if (!ifStmt.hasElsePath)
				{
					// create an else block and put all statements after the if inside it
					auto elsePath = ifStmt.createElsePath(transfers);
					// create a new branch for the else path and move all branches after the if inside it
					auto branch = new Branch(statement.parent.branch,elsePath);
					size_t idx2 = findIndex(node.branch,node.children[1].branch);
					branch.children = node.branch.children[idx2+1..$];
					node.branch.children = node.branch.children[0..idx2+1];
					node.branch.children ~= branch;
					elsePath.propagateBranch(branch); // correct all branch pointers

					// cut out all statements that are now inside the else block
					statement.parent.children.length = idx+1;
					reanalyse(ifStmt.elsePath.getStatementList);
				} else {
					// same as above except the else block already exists
					assert(node.children.length == 4);
					auto branch = node.children[3].branch;
					if (!ifStmt.elsePath.isBlockStatement)
						ifStmt.elsePath.convertToBlockStatement;
					auto list = ifStmt.elsePath.getStatementList;
					list.addChildren(transfers);
					ifStmt.elsePath.propagateBranch(branch);
					statement.parent.children.length = idx+1;
					reanalyse(list);
				}
				return true;
			}
		}
		return false;
	}
	// Inject a `return void 0` at the end of a StatementList whenever
	// there are multiple branches that return a value.
	// This forces the minifier to emit a return statement early on,
	// removing the need for multiple return statements and shortening
	// the code.
	// E.g. function(a){if(a)return 4;if(b)return 5;} => function(a){return a?4:b?5:void 0;}
	// If there is only a single path that returns (non-empty)
	// we don't want to emit a return statement too early, since it takes
	// more characters.
	// E.g. compare: function(a){if(a)return 4;} with function(a){return a?4:void 0;}
	bool insertReturnVoid0WhenMultipleBranchesReturn(Scope s)
	{
		bool hasMoreThanOneBranchReturning(Branch trunk)
		{
			size_t cnt = 0;
			return trunk.someNode!((n){
				if (n.entry.getNthParent(1).isA!"ES6.CaseClause" || n.entry.getNthParent(3).isA!"ES6.CaseBlock") // a CaseClause doesn't need to be examined.
				{
					return Ternary.None;
				}
				if ((n.entry.hint & Hint.Return) || (n.entry.hint & Hint.DeducedReturingBranch))
					cnt++;
				if (n.entry.hint & Hint.DeducedReturingBranch) // a DeducedReturingBranch means that all branches return, and we only count it once. i.e. we stop examining futher nodes.
					return Ternary.None;
				return cnt > 1 ? Ternary.True : Ternary.False;
			}) || cnt > 1;
		}
		assert(s && s.entry && s.entry.branch && s.entry.branch.entry);

		assert(!(s.entry.getNthParent(1).isA!"ES6.CaseClause" || s.entry.getNthParent(3).isA!"ES6.CaseBlock"));
		bool alreadyReturing = (s.entry.getNthChild(2).hint & Hint.Return) || (s.entry.getNthChild(2).hint & Hint.DeducedReturingBranch);
		// only when the function doesn't return (a value) in all cases and the function has multiple branches that return
		if (!alreadyReturing && hasMoreThanOneBranchReturning(s.entry.branch))
		{
			auto list = s.entry.getChild("ES6.StatementList");
			auto void0 = generateReturnVoid0StatementItem();
			void0.parent = list;
			// do we insert a `return void 0` statement.
			list.children ~= void0;
			reanalyse(void0.getNthChild(2));
			return true;
		}
		return false;
	}
	bool combineNestedIfs(Node node)
	{
		void combine(Branch branch, Node assignA, Node assignB)
		{
			combineConditionalExpressions(assignA.getNthChild(2).asExpression,assignB.getNthChild(2).asExpression);
			assignA.children[1] = assignB.children[1];
			assignA.children[1].parent = assignA;
			assignA.children[1].propagateBranch(branch);
			if (branch.children.length > 0)
				branch.children = branch.children[0].children;
			branch.entry = assignA.children[1];
			reanalyse(assignA.children[1]);
		}
		auto ifStmt = node.asIfStatement();
		// when an if statement is the only child of a statementlist in another if statement
		if (!ifStmt.isNull && !ifStmt.hasElsePath)
		{
			auto truthPath = ifStmt.truthPath;
			if (truthPath.isBlockStatement)
			{
				auto list = truthPath.getStatementList;
				if (list !is null && list.children.length > 0)
				{
					auto stm = list.children[0].getNthChild(2);
					if (list.children.length == 1 && stm.isA!"ES6.IfStatement" && stm.children.length == 2)
					{
						auto branch = truthPath.branch;
						combine(branch,node,stm);
						return true;
					}
				}
			} else if (truthPath.children[0].isA!"ES6.IfStatement" && truthPath.children[0].children.length == 2)
			{
				auto branch = truthPath.branch;
				combine(branch,node,truthPath.children[0]);
				return true;
			}
		}
		return false;
	}
	// any !NonValueGeneratingStatement and !EmitReturn statements can be combined into the next for, if or switch
	// but only if the `for` has a VariableDeclarationList
	bool moveNonValueGeneratingStatementsIntoForIfAndSwitches(Node node)
	{
		import std.algorithm : remove;
		// Note: since for, if and switch create a new branch, we can find them quicker by going through the branch tree (Note: for currently don't create branches)
		if (node.isA!"ES6.StatementListItem")
		{
			bool didSomething = false;
			auto ifStmt = node.getNthChild(2);
			if (ifStmt.isA!"ES6.IfStatement")
			{
				auto idx = node.parent.findIndex(node);
				for (auto i = idx-1 ; i >= 0; --i)
				{
					auto item = node.parent.children[i];
					assert(!item.getNthChild(2).isA!"ES6.EmptyStatement"); // this was a if(...) continue, but was pretty sure it never evaluates to true
					if (!(item.hint & Hint.NonValueGeneratingStatement || item.hint & Hint.EmitReturn || item.hint & Hint.VariableDecl || item.hint & Hint.FunctionDecl))
					{
						if (item.getNthChild(2).isA!"ES6.ExpressionStatement")
						{
							auto children = item.getNthChild(3).children;
							children ~= new Node(ParseTree("ES6.Comma",true,[","]));
							ifStmt.children[0].children[0].insertAtFront(children);
						} else
						{
							continue;
							// Note: this is rather ugly since item can be just about anything and we just insert it here without regards to proper grammar...
							//ifStmt.children[0].children[0].insertAtFront([item,new Node(ParseTree("ES6.Comma",true,[","]))]);
						}
						node.parent.children.remove(i);
						node.parent.children = node.parent.children[0..$-1];
						didSomething = true;
					} else
						return didSomething;
				}
				return didSomething;
			}
		}
		return false;
	}
	bool combineExpressionStatementsWithNonEmptyReturnStatements(Node node)
	{
		import std.algorithm : remove;
		if (node.isA!"ES6.StatementListItem")
		{
			bool didSomething = false;
			auto rStmt = node.getNthChild(2);
			if (rStmt.isA!"ES6.ReturnStatement" && (rStmt.hint & Hint.Return))
			{
				auto idx = node.parent.findIndex(node);
				for (auto i = idx-1 ; i >= 0; --i)
				{
					auto item = node.parent.children[i];
					assert(!item.getNthChild(2).isA!"ES6.EmptyStatement"); // this was a if(...) continue, but was pretty sure it never evaluates to true
					if (!(item.hint & Hint.NonValueGeneratingStatement || item.hint & Hint.EmitReturn || item.hint & Hint.VariableDecl || item.hint & Hint.FunctionDecl))
					{
						if (item.getNthChild(2).isA!"ES6.ExpressionStatement")
						{
							auto children = item.getNthChild(3).children;
							children ~= new Node(ParseTree("ES6.Comma",true,[","]));
							rStmt.children[0].insertAtFront(children);
						} else
						{
							continue;
							// Note: this is rather ugly since item can be just about anything and we just insert it here without regards to proper grammar...
							//ifStmt.children[0].children[0].insertAtFront([item,new Node(ParseTree("ES6.Comma",true,[","]))]);
						}
						node.parent.children.remove(i);
						node.parent.children = node.parent.children[0..$-1];
						didSomething = true;
					} else
						return didSomething;
				}
				return didSomething;
			}
		}
		return false;
	}
	bool negateIfContinueStatements(Branch trunk)
	{
		import std.algorithm : remove;
		foreach(branch; trunk.children)
		{
			auto node = branch.entry;
			auto descend = node.getNthChild(1);
			auto ifStmt = node.parent.asIfStatement;
			bool inFunction = node.parent.isA!"ES6.IfStatementReturn";
			if (!inFunction && !node.parent.isA!"ES6.IfStatement")
				continue;
			if (descend.isA!"ES6.BlockStatement")
			{
				auto iterStmt = node.parent.branch.entry.parent;
				if (!iterStmt.isA!"ES6.IterationStatement")
					continue;

				auto list = ifStmt.truthPath.getStatementList;
				if (list is null || !list.keepInStatementListUntilSubStatement!"ES6.ContinueStatement")
					continue;
				/*if (list.children.length == 0)
					list.addChild(new Node(ParseTree("ES6.Statement",true),[new Node(ParseTree("ES6.EmptyStatement",true,[";"]))]));*/
			} else if (!descend.isA!"ES6.ContinueStatement")
				continue;

			auto iterStmt = node.parent.branch.entry.parent;
			if (!iterStmt.isA!"ES6.IterationStatement")
				continue;

			auto blkStmt = node.parent.branch.entry.children[0];
			if (!blkStmt.isA!"ES6.BlockStatement")
			{
				// we can remove continue, but just need to set transfers to []
				continue;
			}

			auto outerList = blkStmt.getNthChild(2);
			auto idx = outerList.findIndex(ifStmt.getNthParent(2));
			auto transfers = outerList.children[idx+1..$];

			if (ifStmt.children.length == 4)
			{
				if (transfers.length > 0)
				{
					// already else case
					auto elseBranch = ifStmt.elsePath.branch;
					if (!ifStmt.elsePath.isBlockStatement)
						ifStmt.elsePath = convertStatementIntoBlockStatement(ifStmt.elsePath);
					auto innerList = ifStmt.elsePath.getStatementList();
					innerList.addChildren(transfers);
					ifStmt.elsePath.propagateBranch(elseBranch);
					outerList.children.length = idx+1;
					if (innerList.children.length == 0)
					{
						innerList.removeFromParent();
						reanalyse(innerList.parent);
					} else
						reanalyse(innerList);
				}
				// keep if path when if blockstmt still has statments
				if (ifStmt.truthPath.isBlockStatement)
				{
					auto list = ifStmt.truthPath.getStatementList();
					if (list.children.length > 0)
						return true;
				}
				// remove truth path, negate if, move else block to truth path
				auto bidx = branch.parent.findIndex(branch);
				branch.entry.parent.negateIfCondition();
				branch.parent.children.remove(bidx);
				branch.parent.children = branch.parent.children[0..$-1];
				ifStmt.truthPath = ifStmt.elsePath;
				ifStmt.truthPath.parent = ifStmt;
				ifStmt.children = ifStmt.children[0..2];
				assert(ifStmt.children.length == 2);
				reanalyse(ifStmt);
				return true;
			} else
			{
				// make sure truth path has block statement
				if (!ifStmt.truthPath.isBlockStatement)
				{
					ifStmt.truthPath = ifStmt.truthPath.convertToBlockStatement;
					branch.entry = ifStmt.truthPath;
				} else
				{
					// keep if path when if blockstmt still has statments
					auto list = ifStmt.truthPath.getStatementList;
					if (list.children.length > 0)
					{
						// TODO: This can be replaced with the createElsePath function, as used in putRemainingStatementsInElseBlockWhenIfReturns below
						auto elseKeyword = new Node(ParseTree("ES6.ElseKeyword",true,["else"]));
						auto elsePath = createStatementListStatement(transfers, inFunction, ifStmt);
						auto ffp = elsePath.getNthChild(3);
						elsePath.scp = branch.entry.scp;
						// create a new branch for the else path and move all branches after the if inside it
						auto elseBranch = new Branch(ifStmt.branch,elsePath);
						size_t idx2 = findIndex(ifStmt.branch,ifStmt.truthPath.branch);
						elseBranch.children = ifStmt.branch.children[idx2+1..$];
						ifStmt.branch.children = ifStmt.branch.children[0..idx2+1];
						ifStmt.branch.children ~= elseBranch;
						elsePath.propagateBranch(elseBranch); // correct all branch pointers

						// insert the new nodes
						ifStmt.addChild(elseKeyword);
						ifStmt.addChild(elsePath);

						// cut out all statements that are now inside the else block
						outerList.children = outerList.children[0..idx+1];
						reanalyse(ffp);
						return true;
					}
				}
				// has no else case
				if (transfers.length > 0)
					ifStmt.negateIfCondition();
				auto innerList = ifStmt.truthPath.getStatementList();
				innerList.setChildren(transfers);
				propagateBranch(ifStmt.truthPath,branch);
				outerList.children = outerList.children[0..idx+1];
				if (innerList.children.length == 0)
				{
					innerList.removeFromParent();
					reanalyse(innerList.parent);
				} else
					reanalyse(innerList);
				return true;
			}
		}
		foreach(branch; trunk.children)
			if (negateIfContinueStatements(branch))
				return true;
		return false;
	}
	bool negateIfContinueStatementsScp(Scope scp)
	{
		if (negateIfContinueStatements(scp.entry.branch))
			return true;
		return false;
	}
	void shortenOftenUsedStringLiterals(Scope scp)
	{
		import std.conv : to;
		struct Stats
		{
			int count;
			ulong length;
			Scope scp;
			size_t depth;
			Node[] literals;
		}
		foreach(s; scp.scopes)
		{
			Node node = s.entry;
			Stats[string] dict;
			void walk(Node node)
			{
				if (node.isA!"ES6.StringLiteral" && !node.parent.isA!"ES6.LiteralPropertyName")
				{
					auto str = node.matches[0][1..$-1];
					if (str != "use strict")
					{
						if (auto p = str in dict)
						{
							if (node.scp !is (*p).scp)
							{
								auto commonScp = findCommonParent((*p).scp,node.scp);
								(*p).scp = commonScp;
								(*p).depth = getScopeDepth(commonScp);
							}
							(*p).literals ~= node;
							(*p).count++;
						}
						else
							dict[str] = Stats(1,str.length,node.scp,getScopeDepth(node.scp),[node]);
					}
				}
				foreach (c; node.children)
					walk(c);
			}
			walk(node);
			foreach (string str, ref Stats stats; dict)
			{
				if (stats.count == 1)
					continue;
				string id = getFreeIdentifierForAllScopesInScope(stats.scp,stats.literals.map!"a.scp");
				//string id = getFreeIdentifier(stats);
				auto len = str.length + 2;
				int charactersSaved = cast(int)(len*stats.count) - cast(int)(2+len+(id.length*(stats.count+1)));
				if (charactersSaved <= 0)
					continue;
				Node list;
				assert(stats.scp.parent !is null);
				list = stats.scp.entry.getChild("ES6.StatementList");
				auto vDeclList = list.getVariableDeclarationList;
				auto decl = createVariableDeclarationNode(id);
				if (vDeclList is null || !vDeclList.isA!"ES6.VariableDeclarationList")
				{
					decl.addChild(createStringLiteralInitializer(str));
					auto item = createVariableDeclarationListStatement([decl]);
					list.insertAtFront(item);
					propagateBranch(item,list.branch);
					reanalyse(decl);
				} else
				{
					decl.addChild(createStringLiteralInitializer(str));
					vDeclList.insertAtFront(decl);
					propagateBranch(decl,vDeclList.branch);
				}
				assert(!stats.scp.hasVariable(id));
				stats.scp.addVariable(Variable(id,IdentifierType.Variable,decl));
				foreach(lit; stats.literals)
				{
					auto iref = createIdentifierReference(id);
					lit.parent.parent.setChild(0,iref);
					propagateBranch(iref,lit.parent.parent.branch);
					lit.scp.addGlobal(id);
				}
			}
		}
	}
	void shortenNullLiterals(Scope scp)
	{
		import std.algorithm : map;
		import std.array : array;
		void getNullLiterals(Sink)(Node node, Sink s)
		{
			if (node.isA!"ES6.NullLiteral")
			{
				s.put(node);
				return;
			}
			foreach (c; node.children)
				getNullLiterals(c,s);
		}
		auto getNullLiteralsAppender(Node node)
		{
			import std.array : appender;
			auto a = appender!(Node[])();
			getNullLiterals(node,a);
			return a.data();
		}
		// find all null literals
		foreach(s; scp.scopes)
		{
			auto nulls = getNullLiteralsAppender(s.entry);
			if (nulls.length < 4)
				continue; 
			auto id = getFreeIdentifierForAllScopesInScope(s, nulls.map!("a.scp").array);
			auto currentLength = nulls.length*4;
			auto hypLength = 4+6+(1+nulls.length)*id.length;
			if (hypLength < currentLength)
			{
				auto list = s.entry.getNthChild(2);
				auto varDecl = createVariableDeclarationNode(id, createInitializer(createUnaryExpressionNullLiteral));
				varDecl.propagateBranch(list.branch);
				list.addVariableDeclarationToStatementList(varDecl);
				s.addVariable(Variable(id,IdentifierType.Variable,varDecl));
				s.addGlobal(id);
				foreach (n; nulls)
				{
					auto idRef = createIdentifierReference(id);
					idRef.propagateBranch(n.branch);
					n.getNthParent(2).setChild(0, idRef);
				}
			}
		}
	}
	bool shortenOftenUsedThisKeyword(Scope scp)
	{
		auto findAllThisKeywords(Node node)
		{
			Node[] ts;
			void walk(Node n)
			{
				if (n.isA!"ES6.ThisKeyword")
					ts ~= n;
				foreach(c; n.children)
					if (!c.isA!"ES6.FunctionDeclaration" && !c.isA!"ES6.FunctionExpression")
						walk(c);
			}
			walk(node);
			return ts;
		}
		Node[] thises = findAllThisKeywords(scp.entry);
		if (thises.length > 3)
		{
			// get free identifier name in this scope
			auto id = getFreeIdentifier(scp);
			// insert `var x = this;`
			auto initializer = createInitializer(createUnaryExpressionPrimaryExpression(new Node(ParseTree("ES6.ThisKeyword",true,["this"]))));
			auto varDecl = createVariableDeclarationNode(id,initializer);
			auto list = scp.getStatementListFromScope();
			list.addVariableDeclarationToStatementList(varDecl,false);
			scp.addVariable(Variable(id,IdentifierType.Variable,varDecl));

			// change all nodes to the new identifier
			foreach (t; thises)
			{
				t.parent.setChild(0,createIdentifierReference(id));
				propagateBranch(t.parent.children[0],t.parent.branch);
				t.scp.addGlobal(id);
			}
			reanalyse(varDecl);
			return true;
		}
		return false;
	}
	bool simplifyEmptyIfStatements(Branch trunk)
	{
		import std.algorithm : all;
		foreach(branch; trunk.children)
		{
			auto node = branch.entry;
			auto ifStmt = node.parent.asIfStatement();
			if (ifStmt.isNull)
				continue;
			auto descend = ifStmt.truthPath.children[0];
			if (ifStmt.truthPath.isBlockStatement)
			{
				auto list = ifStmt.truthPath.getStatementList();
				if (list !is null && list.children.length > 0 && !list.children.all!(c=>c.getNthChild(2).isA!"ES6.EmptyStatement"))
					continue;
			} else if (!ifStmt.truthPath.isEmpty)
				continue;

			// here we have deduced that the if's truth path is empty
			if (ifStmt.hasElsePath)
			{
				descend = ifStmt.elsePath.children[0];
				if (ifStmt.elsePath.isBlockStatement())
				{
					auto list = ifStmt.elsePath.getStatementList();
					if (list.children.length > 0 && !list.children.all!(c=>c.getNthChild(2).isA!"ES6.EmptyStatement"))
					{
						// we can keep the else path, but loose the if path
						ifStmt.negateIfCondition();
						auto truthBranch = ifStmt.truthPath.branch;
						truthBranch.parent.removeChild(truthBranch);
						ifStmt.truthPath = ifStmt.elsePath;
						ifStmt.children = ifStmt.children[0..2];
						reanalyse(ifStmt);
						return true;
					}
				} else if (!ifStmt.elsePath.isEmpty())
				{
					// we can keep the else path, but loose the if path
					assert(false);
				}
			} else
			{
				// lose branch
				ifStmt.parent.children[0] = new Node(ParseTree("ES6.ExpressionStatement",true),
					[
						ifStmt.children[0].children[0]
					],ifStmt.parent);
				branch.parent.removeChild(branch);
				return true;
			}
		}
		foreach(branch; trunk.children)
			if (simplifyEmptyIfStatements(branch))
				return true;
		return false;
	}
	bool simplifyEmptyIfStatementsScp(Scope scp)
	{
		if (simplifyEmptyIfStatements(scp.entry.branch))
			return true;
		return false;
	}
	bool shortenStringifiedNumericLiteralPropertyNames(Node node)
	{
		if (!node.isA!"ES6.LiteralPropertyName")
			return false;
		auto strLit = node.children[0];
		if (!strLit.isA!"ES6.StringLiteral")
			return false;
		if (strLit.matches[0].length == 2)
			return false;
		RawValue raw = RawValue(strLit.matches[0],ValueType.String);
		raw = raw.toNumber;
		if (raw.type != ValueType.Numeric)
			return false;
		node.setChild(0,new Node(ParseTree("ES6.NumericLiteral",true),[
			new Node(ParseTree("ES6.DecimalLiteral",true,[raw.value]))]));
		return true;
	}
	bool removeUnusedParametersScp(Scope scp)
	{
		import std.algorithm : canFind;
		if (scp.entry is null || scp.entry.parent is null)
			return false;
		string[] nonLocalIdentifiers = scp.scopes.getNonLocalIdentifiers();
		auto param = scp.getLastParameter();
		if (param.isNull)
			return false;
		if (scp.hasIdentifier(param.name))
			return false;
		if (nonLocalIdentifiers.canFind(param.name))
			return false;
		scp.removeLastParameter();
		return true;
	}

	void chatty()
	{
		version(Chatty)
		{
			import std.stdio : writeln, write;
			import es5.tojs;
			writeln(root.toJS);
		}
	}
	runOptimizations!(
		removeUnusedParametersScp,
		negateIfContinueStatementsScp,
		combineNestedIfs,
		removeStatementBlockFromSwitchCaseClauses,
		optimizeReturningIfStatement,
		putRemainingStatementsInElseBlockWhenIfReturns,
		insertReturnVoid0WhenMultipleBranchesReturn,
		removeUnnecessaryBreakStatements,
		moveBreakstatementsIntoLoopCondition
	)(root);
	runOptimizations!(
		simplifyEmptyIfStatementsScp,
		optimizeReturningIfStatement,
		combineNestedIfs,
		putRemainingStatementsInElseBlockWhenIfReturns,
		moveNonValueGeneratingStatementsIntoForIfAndSwitches,
		convertIfsToExpressionStatementsOrConditionals,
		parenthesisVoidExpression,
		removeUnnecessaryParentheses,
		shortCircuitIfStatements,
		combineExpressionStatementsWithNonEmptyReturnStatements,
		shortenOftenUsedThisKeyword,
		shortenStringifiedNumericLiteralPropertyNames,
		simplifyRedundantAssignmentExpressions
	)(root);
	shortenOftenUsedStringLiterals(root.scp);
	shortenNullLiterals(root.scp);
	shortenOftenUsedAccessors(root.scp);
	version(Chatty)
	{
		import std.stdio : writeln;
		import es5.tojs;
		writeln(root.toJS);
	}
}
version (unittest)
{
	import unit_threaded;
	unittest
	{
		import std.stdio : writeln;
		import es5.testhelpers;
		void assertTransformation(string input, string expected, in string file = __FILE__, in size_t line = __LINE__)
		{
			import es5.tojs;
			import es6.grammar;
			auto p = ES6(input);
			auto nodes = createNode(p);
			nodes.optimize();
			moveVariableAndFunctionDeclarationsUpFront(nodes);
			bool halt = false;
			auto a = analyseNodes(nodes);
			optimize(nodes,a);
			string got = nodes.toJS();

			if (got != expected)
			{
				Message m;
				m.writeln(nodes);
				m.writeln("Transformation Failure:\nexpected:\n`"~expected~"`\nbut got:\n`"~got~"`");
				failed(m,file,line);
			}
			auto tree = createNode(ES6(expected));
			assertBranchesEqual(a.trunk,analyseNodes(tree).trunk);
			// Note: we can also compare the nodes tree with the tree we get when we ES6 + createNode of expected
			// this way we can make sure that parents, branches, scopes and nodes are equal. 
			// and more importantly we know exactly when they diverge
		}
		assertTransformation(
			`var a`,
			``
		).shouldThrow;
		/// Issue #94
		assertTransformation(
			`var e = "habbahabba"; (function(){ function e() { return "habbahabba"; } if (n == "habbahabba") a = "habbahabba"; })(); (function(){ function e() { return "habbahabba"; } if (n == "habbahabba") a = "habbahabba"; })()`,
			`var e = "habbahabba"; (function (){ var t = "habbahabba"; function e() { return t }; n == t && (a = t) })(); (function (){ var t = "habbahabba"; function e() { return t }; n == t && (a = t) })()`
		);
		/// shortenNullLiterals
		assertTransformation(
			`function e(a, t) { if (a == null || a.p == null) a = t.d || null; if (a == null) return t; return a; }`,
			`function e(a, t) { var n = null; return (a == n || a.p == n) && (a = t.d || n),a == n ? t : a }`
		);
		/// ditto
		assertTransformation(
			`function e(a, t) { function o(n) { if (n) return null; return 7; } if (a == null || a.p == null) a = t.d || null; if (a == null) return t; return a; }`,
			`function e(a, t) { var r = null; function o(n) { return n ? r : 7 }; return (a == r || a.p == r) && (a = t.d || r),a == r ? t : a }`
		);
		// Issue #78
		assertTransformation(
			`var e = window; e.bla = function() { return 5*boo(); } var boo = e.cat = function() { return "hoops" }`,
			`var e = window, boo; e.bla = function (){ return 5 * boo() }; boo = e.cat = function (){ return "hoops" }`
		);
		/// removeUnusedParametersScp
		assertTransformation(
			`function bla(e,a,t){return e*a}`,
			"function bla(e, a) { return e * a }"
		);
		/// ditto
		assertTransformation(
			`function bla(e,a,t){function n() { return 7 * t } return e*a*n()}`,
			"function bla(e, a, t) { function n() { return 7 * t }; return e * a * n() }"
		);
		/// ditto
		assertTransformation(
			`function bla(a,b,c){var d = function () { return 7 * c } return a * b * d() }`,
			"function bla(a, b, c) { var d = function (){ return 7 * c }; return a * b * d() }"
		);
		/// ditto
		assertTransformation(
			`function bla(a, b, c) { return 7 }`,
			"function bla() { return 7 }"
		);
		/// convertIfElseAssignmentToConditionalExpression, optimizeReturningIfStatement and putRemainingStatementsInElseBlockWhenIfReturns
		assertTransformation(
			`function b() { if (a) { d = 5; return; } d = 7 }`,
			`function b() { d = a ? 5 : 7 }`
		);
		/// shortenStringifiedNumericLiteralPropertyNames
		assertTransformation(
			`var a = {"123":123, "": 65 };`,
			`var a = { 123: 123, "": 65 };`
		);
		/// ditto
		assertTransformation(
			`function cd() { if (a) return 7; return 5; }`,
			`function cd() { return a ? 7 : 5 }`
		);
		/// ditto
		assertTransformation(
			`function cd() { if (a) { return 7; } else return 5; }`,
			`function cd() { return a ? 7 : 5 }`
		);
		/// ditto
		assertTransformation(
			`function cd() { if (a) { k(); return 7; } else { p(); return 5; } }`,
			`function cd() { return a ? (k(),7) : (p(),5) }`
		);
		/// ditto
		assertTransformation(
			"function a() { if (a) if (c) return bla; if (b) if (d) return alb; }",
			"function a() { return a && c ? bla : b && d ? alb : void 0 }"
		);
		/// ditto
		assertTransformation(
			"function a() { if (a) { if (d) return 4; if (c) { return bla; } return 7; } }",
			"function a() { if (a) { return d ? 4 : c ? bla : 7 } }"
		);
		/// ditto
		assertTransformation(
			"function a() { if (a) { for(;a < 5;a++)e(); if (d) return 4; if (c) { return bla; } return 7; } }",
			"function a() { if (a) { for(;a < 5;a++)e(); return d ? 4 : c ? bla : 7 } }"
		);
		/// ditto
		assertTransformation(
			"function a() { if (a) { if (d) return 4; if (c) { return bla; } } return 7; }",
			"function a() { if (a) { if (d) return 4; else { if (c) { return bla } } }; return 7 }"
		);
		/// ditto
		assertTransformation(
			"function a() { if (a) { if (d) return 4; if (c) { return bla; } } }",
			"function a() { if (a) { if (d) return 4; else { if (c) { return bla } } }; return void 0 }"
		);
		/// ditto
		assertTransformation(
			"function z(d) { if (a) b ? d() : e(); }",
			"function z(d) { a && (b ? d() : e()) }"
		);
		/// ditto
		assertTransformation(
			"function z(d) { if (a) g = f(); }",
			"function z() { a && (g = f()) }"
		);
		/// ditto
		assertTransformation(
			"function z(d) { if (a) g&&d&&(k=7),f(); }",
			"function z(d) { a && (g && d && (k = 7),f()) }"
		);
		/// ditto
		assertTransformation(
			"function z(d) { if (a) if (b) d() else e() };",
			"function z(d) { a && (b ? d() : e()) }"
		);
		/// shortenOftenUsedStringLiterals
		assertTransformation(
			`var abc; if ("prod" !== "dev") { var someLongName = function b() { return 5; }; }`,
			"var abc, someLongName = function b(){ return 5 };"
		);
		/// ditto
		assertTransformation(
			`function a() { var e = {"ThisStringNeedsToStay": "ThisWillBeReplaced"}, a = {"ThisStringNeedsToStay": "ThisWillBeReplaced"}; }`,
			`function a() { var t = "ThisWillBeReplaced", e = { "ThisStringNeedsToStay": t }, a = { "ThisStringNeedsToStay": t } }`
		);
		/// Issue #42
	  	assertTransformation(
	  		`var a = ("a " + propName + " b ") + ("c.")`,
	        `var a = "a " + propName + " b " + "c.";`
	  	);
		/// shortCircuitIfStatements
		assertTransformation(
			"if (true) d = 5;",
			"d = 5"
		);
		/// ditto
		assertTransformation(
			"if (true) d = 5; else g = 5;",
			"d = 5"
		);
		/// ditto
		assertTransformation(
			"if (a) { if (true) { d = 5; p = 6; } else g = 5; }",
			"a && (d = 5,p = 6)"
		);
		/// ditto
		assertTransformation(
			"if (a) { if (true) { d = 5, p = 6; } else g = 5; }",
			"a && (d = 5,p = 6)"
		);
		/// ditto
		assertTransformation(
			"if (a) if (true) { d = 5; p = 6; } else g = 5;",
			"a && (d = 5,p = 6)"
		);
		/// ditto
		assertTransformation(
			"if (a) if (true) { d = 5, p = 6; } else g = 5;",
			"a && (d = 5,p = 6)"
		);
		/// ditto
		assertTransformation(
			"if (false) d = 5;",
			""
		);
		/// ditto
		assertTransformation(
			"if (false) d = 5; else g = 5;",
			"g = 5"
		);
		/// ditto
		assertTransformation(
			"if (a) if (false) g = 5; else { d = 3; p = 9; }",
			"a && (d = 3,p = 9)"
		);
		/// ditto
		assertTransformation(
			"if (a) { if (false) g = 5; else { d = 4; p = 7; } }",
			"a && (d = 4,p = 7)"
		);
		/// ditto
		assertTransformation(
			"if (!false) d = 5;",
			"d = 5"
		);
		/// ditto
		assertTransformation(
			`if ("abc" !== "def") d = 5;`,
			"d = 5"
		);
		/// ditto
		assertTransformation(
			`"abc" !== "def" ? d = 5 : g = 5;`,
			"d = 5"
		);
		/// ditto
		assertTransformation(
			`"abc" === "def" ? d = 5 : g = 5;`,
			"g = 5"
		);
		/// ditto
		assertTransformation(
			`true && false ? d = 5 : g = 5;`,
			"g = 5"
		);
		/// ditto
		assertTransformation(
			`1 && 0 ? d = 5 : g = 5;`,
			"g = 5"
		);
		/// ditto
		assertTransformation(
			`0 || 1 ? d = 5 : g = 5;`,
			"d = 5"
		);
		/// ditto
		assertTransformation(
			`1 != 2 ? d = 5 : g = 5;`,
			"d = 5"
		);
		/// ditto
		assertTransformation(
			`"abc" === "def" ? d = 5 : g = 5;`,
			"g = 5"
		);
		/// ditto
		assertTransformation(
			`(0 || 1) && (9 == 9) ? d = 5 : g = 5;`,
			"d = 5"
		);
		/// Issue #67
		assertTransformation(
			`if ("use strict", b.can() && top === bottom) doThing();`,
			`"use strict",b.can() && top === bottom && doThing()`
		);
		/// Issue #67
		assertTransformation(
			`if (b.can() && top === bottom && gogo(), true) doThing();`,
			"b.can() && top === bottom && gogo(); doThing()"
		);
		/// Issue #67
		assertTransformation(
			`if (b.can() && top === bottom && gogo(), false) doThing();`,
			"b.can() && top === bottom && gogo()"
		);
		/// Issue #67
		assertTransformation(
			`if (bla(), 6 ? true : false) doThing();`,
			"bla(); doThing()"
		);
		/// Issue #67
		assertTransformation(
			`if (bla(), 0 ? true : false) doThing();`,
			"bla()"
		);
		/// Issue #67
		assertTransformation(
			`if (a) if ("use strict", b.can() && top === bottom) doThing();`,
			`a && ("use strict",b.can() && top === bottom) && doThing()`
		);
		/// Issue #67
		assertTransformation(
			`if (a) if (b.can() && top === bottom && gogo(), true) doThing();`,
			"a && (b.can() && top === bottom && gogo(),doThing())"
		);
		/// Issue #67
		assertTransformation(
			`if (a) if (b.can() && top === bottom && gogo(), false) doThing();`,
			"a && b.can() && top === bottom && gogo()"
		);
		/// Issue #67
		assertTransformation(
			`if (a) if (bla(), 6 ? true : false) doThing();`,
			"a && (bla(),doThing())"
		);
		/// Issue #67
		assertTransformation(
			`if (a) if (bla(), 0 ? true : false) doThing();`,
			"a && bla()"
		);
		/// Issue #67
		assertTransformation(
			`if (a) if (bla(), poi ? kol() : apc()) doThing();`,
			"a && (bla(),poi ? kol() : apc()) && doThing()"
		);
		/// combineExpressionStatementsWithNonEmptyReturnStatements
		assertTransformation(
			"function d(a) { if (a) { if (b) { b = 3; return p; } } }",
			"function d(a) { if (a && b) { return b = 3,p } }"
		);
		/// ditto
		assertTransformation(
			"function d(a) { if (a) { if (b) { e(); return p; } } }",
			"function d(a) { if (a && b) { return e(),p } }"
		);
		/// ditto
		assertTransformation(
			"function d(a) { if (a) { if (b) { if (k) e(); return p; } } }",
			"function d(a) { if (a && b) { return k && e(),p } }"
		);
		/// convertIfsToExpressionStatementsOrConditionals
		assertTransformation(
			"if (a) d = 5;",
			"a && (d = 5)"
		);
		assertTransformation(
			"if (a) d = 5; if (b) g = 5;",
			"a && (d = 5); b && (g = 5)"
		);
		assertTransformation(
			"if (a) {d = 5; e = 5; if (b) g = 5;}",
			"a && (d = 5,e = 5,b && (g = 5))"
		);
		assertTransformation(
			`if (g) { a=5; if (e) d = 5; b = 5;}`,
			"g && (a = 5,e && (d = 5),b = 5)"
		);
		assertTransformation(
			`if (g) { (a = 5,e) && (d = 5); b = 5 }`,
			"g && (a = 5,e && (d = 5),b = 5)"
		);
		assertTransformation(
			`if (g) { if (a=5, e) d = 5; b = 5;}`,
			"g && (a = 5,e && (d = 5),b = 5)"
		);
		/// moveNonValueGeneratingStatementsIntoForIfAndSwitches
		assertTransformation(
			"a = 5; if (d) g = 5;",
			"a = 5; d && (g = 5)"
		);
		/// ditto
		assertTransformation(
			"e(), a = 5; if (d) g = 5;",
			"e(),a = 5; d && (g = 5)"
		);
		/// ditto
		assertTransformation(
			"for (a in k) { d = 5; if (g) k = 3; }",
			"for(a in k){ d = 5; g && (k = 3) }"
		);
		/// combineNestedIfs
		assertTransformation(
			"function a() { if (a) if (c && d) return bla; }",
			"function a() { if (a && c && d) return bla; }"
		);
		/// ditto
		assertTransformation(
			"function a() { if (a) { if (c) { return bla; } } }",
			"function a() { if (a && c) { return bla } }"
		);
		/// ditto
		assertTransformation(
			"function a() { if (a || b) { if (c) { return bla; } } }",
			"function a() { if ((a || b) && c) { return bla } }"
		);
		/// ditto
		assertTransformation(
			"function a() { if (a) { if (b || c) { return bla; } } }",
			"function a() { if (a && (b || c)) { return bla } }"
		);
		/// ditto
		assertTransformation(
			"function a() { if (a || b) { if (c || d) { return bla; } } }",
			"function a() { if ((a || b) && (c || d)) { return bla } }"
		);
		/// ditto
		assertTransformation(
			"function a() { if (a && b) { if (c || d) { return bla; } } }",
			"function a() { if (a && b && (c || d)) { return bla } }"
		);
		/// ditto
		assertTransformation(
			`if (g) if (a=5, e) d = 5;`,
			"g && (a = 5,e) && (d = 5)"
		);
		assertTransformation(
			`(t = 7,g && (a = 5,e)) && (d = 5)`,
			"t = 7,g && (a = 5,e) && (d = 5)"
		);
		/// ditto
		assertTransformation(
			`if (t = 7, g) if (a=5, e) d = 5;`,
			"t = 7,g && (a = 5,e) && (d = 5)"
		);
		/// ditto
		assertTransformation(
			`if (g || t) if (a=5, e) d = 5;`,
			"(g || t) && (a = 5,e) && (d = 5)"
		);
		/// ditto
		assertTransformation(
			`if ((a)) if ((b)) d = 5;`,
			"a && b && (d = 5)"
		);
		/// ditto
		assertTransformation(
			`if (!(a)) if ((!b)) d = 5;`,
			"!a && !b && (d = 5)"
		);
		/// ditto
		assertTransformation(
			`if (!a) if (!b) d = 5;`,
			"!a && !b && (d = 5)"
		);
		/// ditto
		assertTransformation(
			`if (a=5) if (b) d = 5;`,
			"(a = 5) && b && (d = 5)"
		);

		/// optimizeReturningIfStatement
		// Note: Need to test that we don't apply this optimisation if the `if (c) return;` is nested in a if-statement
		assertTransformation(
			`function a() { if (a) return; a = 5; }`,
			`function a() { !a && (a = 5) }`
		);
		/// ditto
		assertTransformation(
			`function a() { if (b) return; }`,
			`function a() { b }`
		);
		/// ditto
		assertTransformation(
			"function d() { if (!a) { return; }; }",
			"function d() { !a }"
		);
		/// ditto
		assertTransformation(
			`function a() { if (a) return; a = 5; if (b) return; a = 7; }`,
			`function a() { !a && (a = 5,!b && (a = 7)) }`
		);
		/// ditto
		assertTransformation(
			`function a() { if (b) { d = 5; return; } e = 5; }`,
			`function a() { b ? d = 5 : e = 5 }`
		);
		/// ditto
		assertTransformation(
			`function a() { if (e) if (b) if (c) return; d = 5; }`,
			`function a() { !(e && b && c) && (d = 5) }`
		);
		/// ditto
		assertTransformation(
			`function a() { if (b) return; else d = 4; }`,
			`function a() { !b && (d = 4) }`
		);
		/// ditto
		assertTransformation(
			`function a() { if (b) return; else d = 4; f = 5;}`,
			`function a() { !b && (d = 4,f = 5) }`
		);
		/// ditto
		assertTransformation(
			`function a() { if (b) { return; } else { d = 4; } f = 5;}`,
			`function a() { !b && (d = 4,f = 5) }`
		);
		/// ditto
		assertTransformation(
			`function a() { if (b) { g = 6; return; } else { d = 4; } f = 5;}`,
			`function a() { b ? g = 6 : (d = 4,f = 5) }`
		);
		/// ditto
		assertTransformation(
			`function a() { if (b) { d = 5; return; } }`,
			`function a() { b && (d = 5) }`
		);
		/// ditto
		assertTransformation(
			`function a() { if (b) { d = 5; return; } else d = 4; f = 5;}`,
			`function a() { b ? d = 5 : (d = 4,f = 5) }`
		);
		/// ditto
		assertTransformation(
			`function a() { for (var k in keys) { if (b) { return; } else d = 4; f = 5; } g = 66; }`,
			`function a() { var k; for(k in keys){ if (b) { return } else  d = 4; f = 5 }; g = 66 }`
		);
		/// ditto
		assertTransformation(
			`function b(z) { if (z.p !== 'value') return; if (z.q === k) return; k = z.v; }`,
			"function b(z) { !(z.p !== 'value') && !(z.q === k) && (k = z.v) }"
		);
		/// ditto
		assertTransformation(
			`function b(z) { if (z.p !== 'value') { return; } if (z.q === k) { return; } k = z.v; }`,
			"function b(z) { !(z.p !== 'value') && !(z.q === k) && (k = z.v) }"
		);
		/// ditto
		assertTransformation(
			`function b() { switch (bla) { case 5: return; }; op() }`,
			`function b() { switch (bla) { case 5: return; }; op() }`
		);
		/// ditto
		assertTransformation(
			`function b() { if (a) for(;;)return; op() }`,
			`function b() { if (a) for(;;)return; op() }`
		);
		/// ditto
		assertTransformation(
			`function b() { if (a) { for(;;)return; } op() }`,
			`function b() { if (a) { for(;;)return }; op() }`
		);
		/// removeStatementBlockFromSwitchCaseClauses
		assertTransformation(
			`switch (a) { case 7: { e(); } };`,
			`switch (a) { case 7: e(); }`
		);
		/// ditto
		assertTransformation(
			`switch (a) { case 7: { e(); } { f(); }};`,
			`switch (a) { case 7: e(); f(); }`
		);
		/// putRemainingStatementsInElseBlockWhenIfReturns
		assertTransformation(
			`function a() { if (a) return 5; d = 6; }`,
			`function a() { if (a) return 5; d = 6 }`
		);
		/// ditto
		assertTransformation(
			`function a() { if (a) return 5; else d = 6; return 4; }`,
			`function a() { return a ? 5 : (d = 6,4) }`
		);
		/// ditto
		assertTransformation(
			`function a() { if (a) return 5; else if (c) d = 6; return 4; }`,
			`function a() { return a ? 5 : (c && (d = 6),4) }`
		);
		/// ditto
		assertTransformation(
			`function a() { if (a) return 5; else if (c) { d = 6; return 4; } return 4; }`,
			`function a() { return a ? 5 : c ? (d = 6,4) : 4 }`
		);
		/// removeEmptyStatementsFromStatementLists
		assertTransformation(
			";;var a = 4;;;",
			"var a = 4;"
		);
		/// ditto
		assertTransformation(
			"var a = 4;;;if (b);;;",
			"var a = 4; b"
		);
		/// shortenOftenUsedStringLiterals
		assertTransformation(
			`var e; function bla() { e = "str"; var t = "doit!"; function gek() { var a = 6; if (t == "doit!") return 17; }} e = "str";`,
			`function bla() { function gek() { var a = 6; if (t == n) return 17; }; var n = "doit!", t = n; e = "str" }; var e; e = "str"`
		);
		/// negateContinueStatements
		assertTransformation(
			`for (var k in keys) { if (b) continue; else doSomething(); }`,
			`var k; for(k in keys){ !b && doSomething() }`
		);
		/// ditto
		assertTransformation(
			`for (var k in keys) { if (b) continue; else { doSomething(); } d = 5;}`,
			`var k; for(k in keys){ !b && (doSomething(),d = 5) }`
		);
		/// ditto
		assertTransformation(
			`for (var k in keys) { if (b) continue; else doSomething(); d = 5;}`,
			`var k; for(k in keys){ !b && (doSomething(),d = 5) }`
		);
		/// ditto
		assertTransformation(
			`for (var k in keys) { if (b) continue; doSomething(); }`,
			`var k; for(k in keys){ !b && doSomething() }`
		);
		/// ditto
		assertTransformation(
			`for (var k in keys) { if (b) {continue}; else doSomething(); }`,
			`var k; for(k in keys){ !b && doSomething() }`
		);
		/// ditto
		assertTransformation(
			`for (var k in keys) { if (b) {continue}; else { doSomething(); } d = 5;}`,
			`var k; for(k in keys){ !b && (doSomething(),d = 5) }`
		);
		/// ditto
		assertTransformation(
			`for (var k in keys) { if (b) {continue}; else doSomething(); d = 5;}`,
			`var k; for(k in keys){ !b && (doSomething(),d = 5) }`
		);
		/// ditto
		assertTransformation(
			`for (var k in keys) { if (b) {continue}; doSomething(); }`,
			`var k; for(k in keys){ !b && doSomething() }`
		);
		/// ditto
		assertTransformation(
			`for (var k in keys) { if (b) {d = 5; continue; } else { doSomething(); } b = 5; }`,
			`var k; for(k in keys){ b ? d = 5 : (doSomething(),b = 5) }`
		);
		/// ditto
		assertTransformation(
			`for (var k in keys) { if (b) {d = 5; continue; } doSomething(); b = 5; }`,
			`var k; for(k in keys){ b ? d = 5 : (doSomething(),b = 5) }`
		);
		/// ditto
		assertTransformation(
			"for (var a in b) { if (!a) { continue; } else { d = 5; }};",
			"var a; for(a in b){ a && (d = 5) }"
		);
		/// ditto
		assertTransformation(
			"for (var a in b) { if (!a) { continue; } d = 5; };",
			"var a; for(a in b){ a && (d = 5) }"
		);
		/// simplifyEmptyIfStatementsScp
		assertTransformation(
			"if (a) { };",
			"a"
		);
		/// ditto
		assertTransformation(
			"if (a) { } else { d = 5 }",
			"!a && (d = 5)"
		);
		/// ditto
		assertTransformation(
			"for (var a in b) { if (!a) { continue; } };",
			"var a; for(a in b){ !a }"
		);
		/// reduceBinaryExpressions
		assertTransformation(
			`var a = "a" + "string" + "that" + "can" + "be" + "concatenated" +
			"even" + "across" + "lines";`,
			`var a = "astringthatcanbeconcatenatedevenacrosslines";`
		);
		/// ditto
		assertTransformation(
			`var a = 5 ^ 7, b = 99 * 12, c = "asdf" + 55 * 9;`,
			`var a = 2, b = 1188, c = "asdf495";`
		);
		/// ditto
		assertTransformation(
			`var a = 5 ^ 7 + unknown(), b = 99 * 12 - unknown, c = "asdf" + 55 * 9 + really.unknown;`,
			`var a = 5 ^ 7 + unknown(), b = 99 * 12 - unknown, c = "asdf" + 55 * 9 + really.unknown;`
		);
		/// ditto
		assertTransformation(
			`if (6 < 7 && 6 * 11 != 0) hallo(); else youWontSeeMe();`,
			`hallo()`
		);
		/// ditto
		assertTransformation(
			`if (5 + 6 + "abc" == "11abc") hallo(); else youWontSeeMe();`,
			`hallo()`
		);
		/// hexOptimisation
		assertTransformation(
			`a = 0x4456;`,
			`a = 17494`
		);
	  	assertTransformation(
	  		`function a(b, c) { if(c) if (c===b) return "b" else return null; switch (b) { case 7: if (b) return null else return c.data } return 4; }`,
	  		`function a(b, c) { if (c) return c === b ? "b" : null; else { switch (b) { case 7: return b ? null : c.data; }; return 4 } }`
	  	);
		/// Issue #58
		assertTransformation(
			`function abc() { for (var propKey in props) { k = props[propKey]; } if (b) { return ret + '>'; } return ret + ' ' + markupForID + '>'; }`,
			"function abc() { var propKey; for(propKey in props){ k = props[ propKey ] }; return b ? ret + '>' : ret + ' ' + markupForID + '>' }"
		);
		/// ditto
		assertTransformation(
			`function abc() { for (var propKey in props) { k = props[propKey]; } if (b) { return ret + '>'; } if (c) return ret + ' ' + markupForID + '>'; }`,
			`function abc() { var propKey; for(propKey in props){ k = props[ propKey ] }; return b ? ret + '>' : c ? ret + ' ' + markupForID + '>' : void 0 }`
		);
		/// ditto
		assertTransformation(
			`function abc() { for (var propKey in props) { k = props[propKey]; } if (b) { return ret + '>'; } for (var i in p) k = p[i]; if (c) return ret + ' ' + markupForID + '>'; if (k) return 77; }`,
			`function abc() { var propKey, i; for(propKey in props){ k = props[ propKey ] }; if (b) { return ret + '>' } else { for(i in p)k = p[ i ]; return c ? ret + ' ' + markupForID + '>' : k ? 77 : void 0 } }`
		);
		assertTransformation(
			`(function b(a,b,c){return 7})({1:[function(_dereq_,module,exports){ somethingSomething() }]})`,
			`(function b(){ return 7 })({ 1: [ function (){ somethingSomething() } ] })`
		);
		assertTransformation(
			`if (a) {  } else {  }`,
			`if (a) {  } else {  }`
		);
	}
}
// Optimizer:
	// function bla() { if (b) c = 5; else if (d) return 2; return 3;}
		/// can to be rewritten into this (it results in shorter minified code):
	// function bla() { if (!b && d) return 2; if (b) c = 5; return 3;}
unittest
{
	import std.stdio : writeln;
	import es5.tojs;
	/// test optimizer
	void assertOptimizer(string js, string expected, in string file = __FILE__, in size_t line = __LINE__)
	{
		import es5.testhelpers;
		auto p = ES6(js);
		auto nodes = createNode(p);
		nodes.optimize();
		try
		{
			auto a = analyseNodes(nodes);
			optimize(nodes,a);
			string got = nodes.toJS();

			if (got == expected)
				return;
			writeln(nodes);
			assert(false,"Optimize Failure:\nexpected:\n"~expected~"\nbut got:\n"~got);
		} catch (Error e)
		{
			Message m;
			m.writeln(nodes);
			m.writeln(e.msg);
			failed(m,file,line);
		}
	}
	assertOptimizer(
		"var a",
		"var z"
	).shouldThrow;
	// here we test the return optimisation
	assertOptimizer(
		"function handly() { if (a) { return; } b = 4; d = 6;}",
		"function handly() { !a && (b = 4,d = 6) }"
	);
	assertOptimizer(
		"function handly() { if (a) return; b = 4; d = 6;}",
		"function handly() { !a && (b = 4,d = 6) }"
	);
	assertOptimizer(
		"function handly() { if (a) return; b = 4; if (a) return; d = 6;}",
		"function handly() { !a && (b = 4,!a && (d = 6)) }"
	);
	assertOptimizer(
		"function handly() { if (!!a) return; b = 4;}",
		"function handly() { !a && (b = 4) }"
	);
	assertOptimizer(
		"function handly() { if (!(a && b)) return; b = 4;}",
		"function handly() { a && b && (b = 4) }"
	);
	assertOptimizer(
		"function handly() { if ((a && b)) return; b = 4;}",
		"function handly() { !(a && b) && (b = 4) }"
	);
	// this can be optimized further into `if (c && !(a && b)) b = 4;`
	assertOptimizer(
		"function handly() { if (c) { if ((a && b)) return; b = 4;}}",
		"function handly() { if (c) { if (a && b) return; b = 4 } }"
	);
	// with this one, the above comment doesn't apply
	assertOptimizer(
		"function handly() { if (c) { if ((a && b)) return; b = 4;} d = 5;}",
		"function handly() { if (c) { if (a && b) return; b = 4 }; d = 5 }"
	);

	// if all paths on an if-statement return, the path the if-statement is on always returns
	assertOptimizer(
		"function bla() { if (b) if (c) return 4; else return 7};",
		"function bla() { if (b) return c ? 4 : 7; }"
	);
	// when an if statement (with no else) returns, we put all statements afterwards in the else block
	assertOptimizer(
		"function bla() { if (b) { if (c) return 4; return 7}};",
		"function bla() { if (b) { return c ? 4 : 7 } }"
	);
	assertOptimizer(
		"function bla() { if (b) if (c) return 4; else return 7; if (d) if (g) return 6; else return 5;}",
		"function bla() { return b ? c ? 4 : 7 : d ? g ? 6 : 5 : void 0 }"
	);
	// ditto
	assertOptimizer(
		"function bla() { if (b) { if (c && bla in key) { return null; } d = 5; return chars; } }",
		"function bla() { if (b) { return c && bla in key ? null : (d = 5,chars) } }"
	);

	assertOptimizer(
		"function bla() { if (b) { return null; } if (c) return 4;}",
		"function bla() { return b ? null : c ? 4 : void 0 }"
	);

	//TODO:  some special return cases
	/*assertOptimizer(
		"function bla(event) { if (d) { if (c) return 5; d = 5;}}",
		"function bla(event){if(d){if(c)return 5;d=5}}"
	);
	assertOptimizer(
		"function bla(event) { if (d) { if (c) return 5;}}"
		"function bla(event){return d&&c?5:void 0};"
	);*/
	// this does not work when a if branch has no return statement
}