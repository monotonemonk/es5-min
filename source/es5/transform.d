module es5.transform;

import es6.grammar;
import es5.node;
import es5.analyse;
import es5.utils;

bool keepInStatementListUntilSubStatement(alias type)(Node list)
{
	assert(list.isA!"ES6.StatementList");
	foreach(cidx, c; list.children)
	{
		if (c.getNthChild(2).isA!type)
		{
			list.children = list.children[0..cidx];
			reanalyse(list);
			return true;
		}
	}
	return false;
}
Node parenthesesExpression(Node c)
{
	assert(c.isA!"ES6.Expression");
	auto expr = new Node(ParseTree("ES6.ExpressionIn",true),c.children);
	auto r = new Node(ParseTree("ES6.ExpressionIn",true),
	[
		parenthesisExprIntoAssignmentExpression(expr)
	]);
	reanalyse(expr);
	return r;
}
void combineConditionalExpressions(Expression a, Expression b)
{
	auto holder = a.parent;
	auto idx = holder.findIndex(a);
	auto exprA = a.getNthChild(4);
	auto exprB = b.getNthChild(4);
	if (a.hint & Hint.LogicalOr || a.isConditionalExpression() || !a.isSingleExpression || !(a.isUnaryExpression() || a.isBinaryExpression()))
	{
		a = parenthesesExpression(a);
		exprA = a.getNthChild(4);
	}
	if (b.hint & Hint.LogicalOr || b.isConditionalExpression() || !b.isSingleExpression || !(b.isUnaryExpression() || b.isBinaryExpression()))
	{
		b = parenthesesExpression(b);
		exprB = b.getNthChild(4);
	}
	import std.stdio;
	assert(exprA !is null && exprB !is null);
	Node binExpr;
	bool aUnaryExpr = a.isUnaryExpression();
	bool bUnaryExpr = b.isUnaryExpression();
	assert(aUnaryExpr || a.isBinaryExpression());
	assert(bUnaryExpr || b.isBinaryExpression());

	if (aUnaryExpr)
	{
		import std.stdio;
		// insert binary expression
		if (bUnaryExpr)
		{
			binExpr = new Node(ParseTree("ES6.BinaryExpressionIn",true),
					[
						exprA,
						createLocigalExpressionOperator!"&&",
						exprB
					]);
		} else
		{
			exprB.insertAtFront([exprA,createLocigalExpressionOperator!"&&"]);
			binExpr = exprB;
		}
	} else
	{
		if (bUnaryExpr)
		{
			exprA.insertAtBack([createLocigalExpressionOperator!"&&",exprB]);
			binExpr = exprA;
		} else
		{
			exprA.addChild(createLocigalExpressionOperator!"&&");
			exprA.addChildren(exprB.children);
			binExpr = exprA;
		}
	}
	auto expr = new Node(ParseTree("ES6.ExpressionIn",true),[new Node(ParseTree("ES6.AssignmentExpressionIn",true),[new Node(ParseTree("ES6.ConditionalExpressionIn"),[new Node(ParseTree("ES6.RightHandSideExpressionIn"),[binExpr])])])]);
	holder.children[idx] = expr;
	expr.parent = holder;
	expr.propagateBranch(holder.branch);
	reanalyse(binExpr);
}

version(unittest)
{
	@("Combine Expressions")
	unittest
	{
		import es5.testhelpers;
		import unit_threaded;
		void assertCombine(string inputA, string inputB, string expected, in string file = __FILE__, in size_t line = __LINE__)
		{
			import std.stdio : writeln;
			import es6.grammar;
			auto pa = ES6(inputA);
			auto nodesA = createNode(pa);
			auto pb = ES6(inputB);
			auto nodesB = createNode(pb);
			analyseNodes(nodesA);
			analyseNodes(nodesB);
			auto exprA = nodesA.getChild("ES6.ExpressionStatement").children[0].asExpression;
			auto exprB = nodesB.getChild("ES6.ExpressionStatement").children[0].asExpression;
			combineConditionalExpressions(exprA,exprB);
			import es5.tojs;
			auto got = nodesA.toJS();
			if (got == expected)
				return;
			Message m;
			m.writeln("Expected");
			m.writeln(expected);
			m.writeln("Got");
			m.writeln(got);
			failed(m,file,line);
		}
		assertCombine(`a`,`b`,`a and b`).shouldThrow();

		assertCombine(`a || b`,`c && d`,"(a || b) && c && d");
		assertCombine(`a && b`,`c || d`,"a && b && (c || d)");

		assertCombine(`a && b`,`d ? e : f`,"a && b && (d ? e : f)");
		assertCombine(`a ? b : c`,`d && e`,"(a ? b : c) && d && e");

		assertCombine(`fun(),a && b`,`c && d`,"(fun(),a && b) && c && d");
		assertCombine(`a && b`,`fun(),c && d`,"a && b && (fun(),c && d)");

		assertCombine(`a = 6`,`b && c`,"(a = 6) && b && c");
		assertCombine(`a && b`,`c = 6`,"a && b && (c = 6)");

		assertCombine(`a`,`b`,"a && b");
		assertCombine(`a`,`b && c`,"a && b && c");
		assertCombine(`a && b`,`c`,"a && b && c");
		assertCombine(`a && b`,`c && d`,"a && b && c && d");

	}
}