module es5.emit;

enum Emit
{
	Normal = 0x1,
	EndingStatement = 0x2,
	Skipped = 0x4,
	ExpectingReturnValue = 0x8,
	InLogicalExpression = 0x10,
	Semicolon = 0x20,
	NoDelimiter = 0x40,
	LastOfBlock = 0x80,
	NotLastOfBlock = 0x100,
	RequireSemicolonDelimiter = 0x200,
	InConditonalStatement = 0x400,
	SingleStatement = 0x800,
	RequireWhiteSpaceForIdentifiers = 0x1000
}

// NOTE: When GDC supports a version of phobos that has BitFlags template defined,
// The `import es5.flags : BitFlags` can be replaced with `import std.typecons : BitFlags`

import es5.flags : BitFlags;
alias EmitState = BitFlags!Emit;