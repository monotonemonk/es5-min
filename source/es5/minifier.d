module es5.minifier;

import std.stdio;
import es6.grammar;
import es5.utils;
import es5.analyse;
import es5.node;
import es5.tojs;
import es5.emit;
import es5.optimize;
import es5.transforms.variables;

unittest
{
	// TODO: there are some variable declaration cleanups we can do

	//"var b,b;";		=>	"var b;";
	//"var a=5,a=15;";  =>  "var a=15";
	//"var a='str',a;";  =>  "var a='str";
	//"var a=e(),a=5;";  =>  "var a=(e(),5)"; (here e() might have side-effects, so we have to call it)
	//"var a=d,a=5;";  =>  "var a=(d,5)"; (here d might have side-effects (property get, symbol, etc. (is that true?)) so we have to call it)
}
auto minify(Node root)
{
	import std.array : appender;
	auto app = appender!string;
	minified(root, app);
	return app.data;
}
bool anyHasHint(Node[] ht, Hint needle)
{
	foreach(h; ht)
		if (h.hint & needle)
			return true;
	return false;
}
unittest
{
	auto a = new Node(ParseTree("Bla",true));
	auto b = new Node(ParseTree("Bla",true));
	a.hint |= Hint.Return;
	b.hint |= Hint.Break | Hint.Literal;
	assert([a,b].anyHasHint(Hint.Return));
	assert(![a,b].anyHasHint(Hint.DeducedReturingBranch));
}
bool not(EmitState state, Emit flag)
{
	return (state & flag) == EmitState.init;
}
bool isNonReducableIfStatement(Node node, EmitState state)
{
	return (!(state & Emit.ExpectingReturnValue) && node.hint & Hint.EmitReturn) || node.hint & Hint.NonValueGeneratingStatement || (!(node.hint & Hint.DeducedReturingBranch) && !(state & Emit.ExpectingReturnValue) && (node.children[1].hint & Hint.Return || node.hint & Hint.EmptyReturn));
}
bool leadsToNonReducableIfStatementWithoutElse(Node node, EmitState state)
{
	if (node.isA!"ES6.BlockStatement")
	{
		auto list = node.getStatementListFromBlockStatement();
		if (list.children.length > 1)
			return false;
		return list.getNthChild(3).leadsToNonReducableIfStatementWithoutElse(state);
	} else if (node.isA!"ES6.BreakableStatement")
	{
		auto iter = node.children[0].asIterationStatement;
		if (iter.isNull || iter.isDoWhileLoop)
			return false;
		auto child = iter.children[$-1].children[0];
		return child.leadsToNonReducableIfStatementWithoutElse(state);
	} else if (node.isA!"ES6.IfStatement")
	{
		auto ifStmt = node.asIfStatement();
		assert(ifStmt.isNonReducableIfStatement(state));
		if (ifStmt.hasElsePath)
		{
			if (ifStmt.truthPath.doesStatementReturn)
				return true;
			return ifStmt.elsePath.children[0].leadsToNonReducableIfStatementWithoutElse(state);
		}
		return true;
	}
	return false;
}
bool doesStatementReturn(Node node)
{
	return node.hint & Hint.Return || node.hint & Hint.EmptyReturn || node.hint & Hint.DeducedReturingBranch;
}
void minified(Output)(Node node, Output o)
{
	void outputUseStrict(Node)(Node node)
	{
		if (node.matches.length > 0 && node.matches[0].length > 2 && node.matches[0][1..$-1] == "use strict")
			o.put("\"use strict\";");
	}
	EmitState parseStatementList(alias Open, alias Close, alias Delimiter)(Node node, bool skipParenthesisForOneElement, EmitState emit = EmitState(Emit.Normal), EmitState emitSkipParenthesis = EmitState(Emit.Normal))
	{
		import std.algorithm : any;
		assert(node.isA!"ES6.StatementList");
		if (node.children.length == 0)
			return EmitState(Emit.Normal);
		bool avoidOpenAndCloseChars = skipParenthesisForOneElement && node.children.length == 1;
		if (avoidOpenAndCloseChars)
		{
			emit |= emitSkipParenthesis;
			auto r = parseToCode(node.children[0],emit);
			if (r & Emit.ExpectingReturnValue)
				o.put("void 0");
			if (r & Emit.Semicolon)
				return r;
			return r | Emit.RequireSemicolonDelimiter;
		}
		o.put(Open);
		static if (Open == "{")
			emit &= ~Emit.SingleStatement;
		static if (Delimiter == "")
			return parseChildrenDelimited!Delimiter(node.children,emit);
		else
		{
			if (parseChildrenDelimited!Delimiter(node.children,emit) & Emit.ExpectingReturnValue)
				o.put("void 0");
			o.put(Close);
			static if (Close == "}")
				return EmitState(Emit.Semicolon);
			else
				return EmitState(Emit.Normal);
		}
	}
	EmitState parseChildrenDelimited(alias Delimiter)(Node[] children, EmitState emit = EmitState(Emit.Normal))
	{
		if (children.length == 0)
			return EmitState(Emit.Normal);
		auto node = children[0].parent;
		bool emitReturn = (emit & Emit.ExpectingReturnValue) ? true : false;
		bool wantToEmitReturn = !emitReturn && node !is null && (node.hint & Hint.Return) && !(node.hint & Hint.EmptyReturn) && node.isA!"ES6.StatementList";
		bool canEmitReturn = !(node.hint & Hint.NonValueGeneratingStatement);
		EmitState r = Emit.Skipped;
		foreach(idx,child; children[0..$-1])
		{
			bool skipEmitReturn = child.hint & Hint.VariableDecl || child.hint & Hint.FunctionDecl;
			if (!emitReturn && !skipEmitReturn && wantToEmitReturn && canEmitReturn)
			{
				o.put("return");
				emit |= Emit.ExpectingReturnValue | Emit.RequireWhiteSpaceForIdentifiers;
				emitReturn = true;
			}
			auto s = parseToCode(child,emit | Emit.NotLastOfBlock);
			if ((node.isA!"ES6.StatementList" || node.isA!"ES6.ModuleItemList") && s & Emit.RequireSemicolonDelimiter)
			{
				o.put(";");
				s |= Emit.Semicolon;
			}
			if ((s & Emit.Skipped) || (s & Emit.EndingStatement) || (s & Emit.NoDelimiter))
				continue;
			assert(!(s & Emit.ExpectingReturnValue)); // this should all be handled by the optimiser
			if (s & Emit.Semicolon)
				continue;
			r |= Emit.Normal;
			if ((node.isA!"ES6.StatementList" && (children[idx+1].hint & Hint.Break)) || (children[idx+1].hint & Hint.VariableDecl) || ((node.isA!"ES6.StatementList" || node.isA!"ES6.ModuleItemList") && (children[idx+1].hint & Hint.NonValueGeneratingStatement)))
				o.put(";");
			else
			{
				static if (Delimiter == ";")
				{
					if (emitReturn)
						o.put(",");
					else
						o.put(";");
				} else
					o.put(Delimiter);
			}
		}
		auto last = children[$-1];
		if (!emitReturn && wantToEmitReturn && canEmitReturn)
		{
			o.put("return");
			emit |= Emit.ExpectingReturnValue | Emit.RequireWhiteSpaceForIdentifiers;
			emitReturn = true;
		}
		if (node !is null && node.isA!"ES6.StatementList")
			emit |= Emit.LastOfBlock;
		auto s = parseToCode(last,emit);
		if (s & Emit.RequireSemicolonDelimiter)
			r |= Emit.RequireSemicolonDelimiter;
		if (s & Emit.ExpectingReturnValue && !(emit & Emit.NotLastOfBlock))
		{
			o.put("void 0");
			return EmitState(Emit.Normal);
		}
		else if (s & Emit.Semicolon)
			return EmitState(Emit.Semicolon);
		else if (s.not(Emit.Skipped) && s.not(Emit.EndingStatement) && s.not(Emit.NoDelimiter))
			return s;
		return r;
	}
	int ident = 0;
	EmitState parseToCode(Node node, EmitState state = EmitState(Emit.Normal))
	{
		if (node is null)
			return EmitState(Emit.Normal);
		EmitState parseChildren(Node[] children, EmitState state = EmitState(Emit.Normal))
		{
			return parseChildrenDelimited!""(children, state);
		}
		EmitState r = Emit.Normal;
		import std.algorithm : canFind;
		if (node.isA!"ES6.EmptyStatement")
		{
			o.put(";");
			return EmitState(Emit.Semicolon);
		}
		if (node.name.canFind("ES6.CurlyBrackets"))
		{
			if (node.children.length > 0 && node.children[0].isA!"ES6.StatementList" && node.children[0].children.length == 1)
				return parseChildren(node.children,state);
			o.put("{");
			r = parseChildren(node.children,state) & ~Emit.RequireSemicolonDelimiter;
			o.put("}");
			r |= Emit.Semicolon;
			return r;
		}
		if (node.name.canFind("ES6.Parentheses"))
		{
			o.put("(");
			r = parseChildren(node.children,state);
			o.put(")");
			return r;
		}
		if (node.name.canFind("ES6.SquareBrackets"))
		{
			o.put("[");
			r = parseChildren(node.children,state);
			o.put("]");
			return r;
		}
		if (node.name.canFind("ES6.BinaryExpression"))
		{
			auto children = node.children;
			do
			{
				if (children.length == 1)
				{
					parseToCode(children[0],state);
					break;
				}
				if (children[1].children[0].name.canFind("ES6.LogicalOperator"))
				{
					parseToCode(children[0],state);
					parseToCode(children[1],state);
					children = children[2..$];
					continue;
				} else
				{
					parseToCode(children[0],state);
					parseToCode(children[1],state);
					parseToCode(children[2],state);
				}
				if (children.length > 3)
				{
					parseToCode(children[3],state);
					children = children[4..$];
				} else
					break;
			} while (children.length > 0);
			return EmitState(Emit.Normal);
		}
		switch(node.name)
		{
			case "ES6.FunctionDeclaration":
			case "ES6.FunctionDeclarationYield":
			case "ES6.FunctionDeclarationDefault":
			case "ES6.FunctionDeclarationYieldDefault":
			{
				o.put("function ");
				foreach(child; node.children)
					parseToCode(child);
				return EmitState(Emit.Semicolon);
			}
			case "ES6.VariableStatement":
			{
				o.put("var ");
				return parseToCode(node.children[0],state);
			}
			case "ES6.VariableDeclarationListIn":
			{
				if (node.children.length == 0)
					return EmitState(Emit.Normal);
				foreach(p; node.children[0..$-1])
				{
					parseToCode(p,state);
					o.put(",");
				}
				parseToCode(node.children[$-1],state);
				return EmitState(Emit.RequireSemicolonDelimiter);
			}
			case "ES6.RelationalOperatorIn":
			{
				if (node.matches[0] == "in")
					o.put(" in ");
				else if (node.matches[0] == "instanceof")
					o.put(" instanceof ");
				else
					o.put(node.matches[0]);
			} break;
			case "ES6.ModuleBody":
			{
				auto f = parseChildren(node.children,state);
				auto c = node.getChild("ES6.ModuleItemList");
				if (f & Emit.RequireSemicolonDelimiter)
					o.put(";");
				else if (!(f & Emit.Semicolon) && !(f & Emit.NoDelimiter) && c !is null)
				{
					if (c.children[$-1].isA!"ES6.StatementListItem")
					{
						if (!(c.children[$-1].children[0].isA!"ES6.Statement" &&
							c.children[$-1].children[0].children[0].isA!"ES6.VariableStatement"))
							o.put(";");
					}
				}
			} break;
			/*case "ES6.FromClause":
			{
				o.put(" from ");
				return parseToCode(node.children[0],state);
			}*/
			case "ES6.DefaultClauseReturn":
			case "ES6.DefaultClause":
			{
				o.put("default:");
				parseChildren(node.children,state);
				return EmitState(Emit.RequireSemicolonDelimiter);
			}
			case "ES6.CaseBlock":
			case "ES6.CaseBlockReturn":
			{
				o.put("{");
				auto cs = node.children[0].children;
				foreach (c; cs[0..$-1])
				{
					if (parseToCode(c,state) & Emit.RequireSemicolonDelimiter)
						o.put(";");
				}
				parseToCode(cs[$-1]);
				o.put("}");
				return EmitState(Emit.Semicolon);
			}
			case "ES6.CaseClauses":
			case "ES6.CaseClausesReturn":
			{
				foreach (c; node.children[0..$-1])
				{
					if (parseToCode(c,state) & Emit.RequireSemicolonDelimiter)
						o.put(";");
				}
				r = parseToCode(node.children[$-1]);
				if (r & Emit.Semicolon)
					return r;
				return r | Emit.RequireSemicolonDelimiter;
			}
			case "ES6.SwitchStatement":
			case "ES6.SwitchStatementReturn":
			{
				o.put("switch");
				return parseChildren(node.children,state);
			}
			case "ES6.CaseClause":
			case "ES6.CaseClauseReturn":
			{
				state &= ~Emit.SingleStatement;
				o.put("case ");
				parseToCode(node.children[0],state);
				o.put(":");
				if (node.children.length == 1)
					return r;
				r = parseChildren(node.children[1..$],state);
				if (r & Emit.Semicolon)
					return r;
				return EmitState(Emit.RequireSemicolonDelimiter);
			}
			/*case "ES6.ImportDeclaration":
			{
				o.put("import ");
				parseChildren(node.children,state);
				o.put(";");
			} break;*/
			case "ES6.Expression":
			case "ES6.ExpressionIn":
			{
				if (state & Emit.RequireWhiteSpaceForIdentifiers)
				{
					state &= ~Emit.RequireWhiteSpaceForIdentifiers;
					auto expr = node.asExpression();
					auto prefExpr = expr.getFirstPrefixExpression();
					if (	!expr.isParenthesedExpression &&
							!expr.isStringLiteral &&
							(prefExpr.isNull || 
							!(
								prefExpr.matches[0][0] == '-' ||
								prefExpr.matches[0][0] == '+' ||
								prefExpr.matches[0][0] == '!'
							))
						)
					{
						o.put(" ");
					}
				}
				return parseChildren(node.children,state);
			}
			case "ES6.ArgumentList":
			case "ES6.ElementList":
			case "ES6.ImportsList":
			case "ES6.FormalsList":
			{
				return parseChildrenDelimited!(",")(node.children,state);
			}
			case "ES6.CallExpression":
			case "ES6.CallExpressionYield":
			{
				parseChildren(node.children,state);
				return EmitState(Emit.RequireSemicolonDelimiter);
			}
			/*case "ES6.SuperCall":
			{
				o.put("super");
				parseChildren(node.children,state);
			} break;
			case "ES6.ClassHeritage":
			{
				o.put(" extends ");
				parseChildren(node.children,state);
			} break;
			case "ES6.ClassDeclaration":
			{
				o.put("class ");
				parseChildren(node.children,state);
			} break;
			case "ES6.MethodDefinition":*/
			case "ES6.FunctionExpression":
			{
				o.put("function");
				if (node.children[0].isA!"ES6.BindingIdentifier")
					o.put(" ");
				r = parseChildren(node.children);
			} break;
			case "ES6.FormalParameterList":
			case "ES6.FormalParameterListYield":
			{
				return parseChildrenDelimited!(",")(node.children,state);
			}
			case "ES6.PrefixExpression":
			{
				o.put(node.matches[0]);
				if (node.matches[0][0] == 't' || node.matches[0][0] == 'd' || node.matches[0][0] == 'v') // typeof or delete or void
					o.put(" ");
			} break;
			case "ES6.ReturnStatement":
			{
				if (node.children.length == 0)
				{
					o.put("return");
					return EmitState(Emit.RequireSemicolonDelimiter);
				}
				r = EmitState(Emit.RequireSemicolonDelimiter);
				if (state.not(Emit.ExpectingReturnValue))
				{
					o.put("return");
				}
				r |= parseChildren(node.children,state | Emit.RequireWhiteSpaceForIdentifiers);
				return r;
			}
			case "ES6.PropertyDefinitionList":
			{
				return parseChildrenDelimited!(",")(node.children,state);
			}
			case "ES6.PropertyDefinition":
			{
				if (node.children[0].isA!"ES6.PropertyName")
				{
					parseToCode(node.children[0],state);
					o.put(":");
					parseToCode(node.children[1],state);
				} else
					return parseToCode(node.children[0],state);
			} break;
			case "ES6.BooleanLiteral":
			{
				if (node.matches[0][0] == 't')
					o.put("!0");
				else
					o.put("!1");
			} break;
			case "ES6.StatementListItem":
			case "ES6.StatementListItemReturn":
			{
				if (node.matches.length > 0 && node.matches[0].length > 2 && node.matches[0][1..$-1] == "use strict")
				{
					return EmitState(Emit.Skipped);
				}
				r = parseToCode(node.children[0],state);
			} break;
			case "ES6.ModuleItemList":
			{
				return parseChildrenDelimited!(",")(node.children,state);
			}
			case "ES6.StatementList":
			case "ES6.StatementListReturn":
			{
				return parseChildrenDelimited!(";")(node.children,state);
			}
			case "ES6.FieldAccessor":
			{
				o.put(".");
				return parseToCode(node.children[0],state);
			}
			case "ES6.FunctionBody":
			{
				return parseToCode(node.children[0],state);
			}
			case "ES6.IfStatement":
			case "ES6.IfStatementYield":
			case "ES6.IfStatementReturn":
			case "ES6.IfStatementYieldReturn":
			{
				auto ifStmt = node.asIfStatement();
				auto truthPath = ifStmt.truthPath;
				bool emitTernaryBlock = node.children.length > 2 || (node.hint & Hint.DeducedReturingBranch);
				assert(ifStmt.isNonReducableIfStatement(state));
				if (emitTernaryBlock)
				{
					auto statementList = node.parent.parent.parent;
					// when we expect a returning value we can never put an `if`, however, if the statementList has a NonValueGeneratingStatement we emit the if thing
					//auto generateNonValueEmittingIfStatement = !(state & Emit.ExpectingReturnValue) && node.children[1].hint & Hint.Return;
					o.put("if(");
					parseToCode(ifStmt.condition.children[0],state);
					o.put(")");
					bool emitSemicolon = false;
					if (truthPath.isBlockStatement())
					{
						bool canOmitBrackets = true;
						auto list = truthPath.getStatementList;
						if (ifStmt.hasElsePath && list.children.length == 1)
						{
							// if there is an else case, we have to make sure there are brackets when the if condition leads to another if (that has no else statement), otherwise the else statement is ambiguous
							auto statement = list.getNthChild(2);
							if (statement.isA!"ES6.Statement")
							{
								canOmitBrackets = !statement.children[0].leadsToNonReducableIfStatementWithoutElse(state);
							}
						}
						emitSemicolon = cast(bool)(parseStatementList!("{","}",";")(list,canOmitBrackets,state,EmitState(Emit.SingleStatement)) & Emit.RequireSemicolonDelimiter);
					}
					else
					{
						parseToCode(truthPath,state | Emit.SingleStatement);
						emitSemicolon = true;
					}
					if (emitSemicolon)
						o.put(";");
					auto elsePath = ifStmt.elsePath;
					bool emitElse = false;
					bool canOmitElse = truthPath.doesStatementReturn && !(state & Emit.SingleStatement);
					if (!canOmitElse)
					{
						emitElse = true;
						o.put("else");
					}
					if (elsePath.isBlockStatement)
					{
						auto sl = elsePath.getStatementList;
						if (sl.children.length == 1)
						{
							if (emitElse) o.put(" ");
							return parseStatementList!("","","")(elsePath.getNthChild(4),true,state);
						} else if (emitElse)
							return parseStatementList!("{","}",";")(sl,true,state);
						return parseStatementList!("","",";")(sl,true,state);
					}
					if (emitElse)
						o.put(" ");
					if (!canOmitElse)
						state |= Emit.SingleStatement;
					return parseToCode(elsePath,state);
				} else
				{
					auto statementList = node.parent.parent.parent;
					//auto generateNonValueEmittingIfStatement =  && node.children[1].hint & Hint.Return;
					o.put("if(");
					parseToCode(ifStmt.condition.children[0],state);
					o.put(")");
					bool emitSemicolon = false;

					r = EmitState(Emit.EndingStatement);
					if (truthPath.isBlockStatement)
						r |= parseStatementList!("{","}",";")(truthPath.getStatementList,true,state,EmitState(Emit.SingleStatement));
					else
					{
						state |= Emit.SingleStatement;
						r |= parseToCode(truthPath,state);
					}
					return r;
				}
			} //break;
			/*case "ES6.ArrowParameters":
			case "ES6.ArrowParametersYield":
			{
				r = parseChildren(node.children,state);
				o.put("=>");
			} break;*/
			case "ES6.Initializer": case "ES6.InitializerIn":
			case "ES6.InitializerYield":
			case "ES6.InitializerInYield":
			{
				o.put("=");
				return parseToCode(node.children[0],state);
			}
			/*case "ES6.LetOrConst":
			{
				o.put(node.matches[0]);
				o.put(" ");
			} break;*/
			case "ES6.InKeyword":
				o.put(" in ");
				break;
			case "ES6.OfKeyword":
				o.put(" of ");
				break;
			case "ES6.NewKeyword":
				o.put("new ");
				break;
			case "ES6.ConditionalExpression":
			case "ES6.ConditionalExpressionIn":
			case "ES6.ConditionalExpressionYield":
			case "ES6.ConditionalExpressionInYield":
			{
				parseToCode(node.children[0],state);
				if (node.children.length > 1)
				{
					o.put("?");
					parseToCode(node.children[1],state);
					o.put(":");
					parseToCode(node.children[2],state);
				}
			} break;
			case "ES6.RegularExpressionLiteral":
			{
				o.put('/');
				parseToCode(node.children[0]);
				o.put('/');
				if (node.children.length == 2)
					parseToCode(node.children[1]);
				return r;
			}
			case "ES6.RegularExpressionClass":
			{
				o.put('[');
				parseToCode(node.children[0]);
				o.put(']');
				return r;
			}
			case "ES6.RegularExpressionBackslashSequence":
			{
				o.put("\\");
				return parseToCode(node.children[0]);
			}
			case "ES6.TryStatement":
			case "ES6.TryStatementReturn":
			{
				o.put("try{");
				parseToCode(node.getNthChild(3));
				o.put("}");
				parseToCode(node.children[1]);
				parseChildren(node.children[2..$]);
				return EmitState(Emit.Semicolon);
			}
			case "ES6.Catch":
			case "ES6.CatchReturn":
			{
				o.put("catch");
				parseToCode(node.children[0]);
				o.put('{');
				parseToCode(node.children[1].getNthChild(2));
				o.put('}');
				return EmitState(Emit.Semicolon);
			}
			case "ES6.ThrowStatement":
			{
				o.put("throw");
				parseToCode(node.children[0],EmitState(Emit.RequireWhiteSpaceForIdentifiers));
				return EmitState(Emit.RequireSemicolonDelimiter);
			}
			case "ES6.Finally":
			case "ES6.FinallyReturn":
			{
				o.put("finally{");
				parseToCode(node.getNthChild(3));
				o.put("}");
				return EmitState(Emit.Semicolon);
			}
			case "ES6.BreakStatement":
			case "ES6.BreakStatementYield":
			{
				o.put("break");
				return EmitState(Emit.RequireSemicolonDelimiter);
			}
			case "ES6.IterationStatement":
			case "ES6.IterationStatementYield":
			case "ES6.IterationStatementReturn":
			case "ES6.IterationStatementYieldReturn":
			{
				auto iter = node.asIterationStatement;

				if (iter.isForLoop)
				{
					o.put("for");
					parseToCode(node.children[1],state);
					r = parseToCode(node.children[2],state);
					return r;
				}
				if (!iter.isDoWhileIteration)
					goto default;
				o.put("do ");

				if (iter.hasBlockStatement)
				{
					auto list = iter.getStatementList();
					if (list.children.length > 1)
						o.put("{");
					parseToCode(list,state);
					if (list.children.length > 1)
						o.put("}");
					else
						o.put(";");
				} else
				{
					parseToCode(iter.getStatement,state);
					o.put(";");
				}
				o.put("while");
				parseToCode(iter.children[3],state);
				return EmitState(Emit.RequireSemicolonDelimiter);
			}
			case "ES6.IdentifierName":
			{
				auto name = node.matches[0];
				if (name == "undefined")
					o.put("void 0");
				else if (name == "Infinity")
					o.put("1/0");
				else o.put(name);
			} break;
			default:
				if (node.children.length == 0)
				{
					if (node.matches.length == 0)
						return EmitState(Emit.Skipped);
					foreach(m; node.matches)
						o.put(m);
				}
				else
					return parseChildren(node.children,state);
		}
		return r;
	}
	outputUseStrict(node);
	parseToCode(node);
}
version (unittest)
{
	import unit_threaded;
	unittest
	{
		import es5.testhelpers;
		/// test emitter
		void assertMinifier(string js, string expected, in string file = __FILE__, in size_t line = __LINE__)
		{
			try
			{
				auto p = ES6(js);
				auto nodes = createNode(p);
				nodes.optimize();
				moveVariableAndFunctionDeclarationsUpFront(nodes);
				auto a = analyseNodes(nodes);
				optimize(nodes,a);
				a.scp.shortenVariables();
				string got = nodes.minify();
				if (got == expected)
					return;
				Message m;
				m.writeln(nodes);
				m.writeln("Minifier Failure:\nexpected:\n\t`"~expected~"`\nbut got:\n\t`"~got~"`");
				failed(m,file,line);
			} catch (Throwable e)
			{
				Message m;
				m.writeln(e);
				failed(m,file,line);
			}
		}
		assertMinifier("var a = 7","").shouldThrow();
		assertMinifier(
			`function a(){return -1}`,
			`function a(){return-1}`
		);
		assertMinifier(
			`function a(){return !1}`,
			`function a(){return!1}`
		);
		assertMinifier(
			`function a(){return (a||b)&&d}`,
			`function a(){return(a||b)&&d}`
		);
		assertMinifier(
			`function a(){return "str"}`,
			`function a(){return"str"}`
		);
		assertMinifier(
			`function a(){return v}`,
			`function a(){return v}`
		);
		/// shortenOftenUsedThisKeyword
		assertMinifier(
			`function b() { var e = this.b; var n = function() { this.that = this.those; this.these = this.there; }; this.doBla(); if(this.prototype.constructor == "as") this.goGreen(); function t() { this.abc = this.createAbc(); this.next = this.goNext();}}`,
			`function b(){function t(){var e=this;e.abc=e.createAbc();e.next=e.goNext()}var a=this,e=a.b,n=function(){var e=this;e.that=e.those;e.these=e.there};a.doBla();a.prototype.constructor=="as"&&a.goGreen()}`
		);
		assertMinifier(
			`function c(e) { function n() { this.bla(); this.green = this.red; this.goGo(); e(); } }`,
			`function c(e){function a(){var a=this;a.bla();a.green=a.red;a.goGo();e()}}`
		);
		assertMinifier(
			`function c(e) { function n() { this.bla(); this.green = this.red; this.goGo(); e.call(this); } }`,
			`function c(e){function a(){var a=this;a.bla();a.green=a.red;a.goGo();e.call(a)}}`
		);
		assertMinifier(
			"var e = 6; function hup() { var abc=3,def=4,klo=7; function dup() { var ghi=4,jkl=5; return ghi*def*jkl*a*b*klo; } function gup() { function dop() { var zzz = 7; return zzz*a*abc*def; }; var mno = 6; return mno; } }",
			`function hup(){function o(){var t=4,o=5;return t*e*o*a*b*n}function r(){function o(){var n=7;return n*a*t*e}var n=6;return n}var t=3,e=4,n=7}var e=6;`
		);
		/// combineExpressionStatementsWithNonEmptyReturnStatements
		assertMinifier(
			"function d(e) { if (e) { if (a) { a = 3; return p; } } }",
			"function d(e){if(e&&a)return a=3,p}"
		);
		/// ditto
		assertMinifier(
			"function d(a) { if (a) { if (b) { e(); return p; } } }",
			"function d(a){if(a&&b)return e(),p}"
		);
		/// ditto
		assertMinifier(
			"function d(a) { if (a) { if (b) { if (k) e(); return p; } } }",
			"function d(a){if(a&&b)return k&&e(),p}"
		);
		/// ditto
		assertMinifier(
			`function a() { if (a) { d = 5; return 5; } else d = 6; return 4; }`,
			`function a(){return a?(d=5,5):(d=6,4)}`
		);
		/// ditto
		assertMinifier(
			`function a() { if (a) return 5; else if (c) d = 6; return 4; }`,
			`function a(){return a?5:(c&&(d=6),4)}`
		);
		assertMinifier(
			"function name(e) { for (i = 0; 10 > i; i++) { if (e) break; (g === d.type || k === d.type) && (j = 5) }; if (f) d=5; }",
			`function name(e){for(i=0;10>i&&!e;i++)(g===d.type||k===d.type)&&(j=5);f&&(d=5)}`
		);
		// function and variables names cannot be minified in global scope, only in functions
		assertMinifier(
			"function hup() { var foo = 5; return foo; }; var bla = hup();",
			"function hup(){var e=5;return e}var bla=hup();"
		);
		assertMinifier(
			"function hup(event) { var e = 5; return e; };",
			"function hup(){var e=5;return e}"
		);
		// check that we reduce (nested) if/else statements/statement-blocks
		assertMinifier(
			"if (46 == a && 5 > b) c = 56;",
			"46==a&&5>b&&(c=56);"
		);
		assertMinifier(
			"if (46 == a && 5 > b) c = 56; else d = 45;",
			"46==a&&5>b?c=56:d=45;"
		);
		assertMinifier(
			"if (a == 46 && b > 4) c = 56; else if (ee == 5) d = 45;",
			"a==46&&b>4?c=56:ee==5&&(d=45);"
		);
		// ifs with only an assignment statement need parenthesis
		assertMinifier(
			"function handleTouch(event) { if (b) if (c) a=5; }",
			"function handleTouch(){b&&c&&(a=5)}"
		);
		// ifs with only function call need no parenthesis
		assertMinifier(
			"function handleTouch(event) { if (c) if (d) event(); }",
			"function handleTouch(e){c&&d&&e()}"
		);
		// watch out for operator precedence
		assertMinifier(
			"function z(d) { if (a) if (b) d() else e() };",
			"function z(t){a&&(b?t():e())}"
		);
		// code blocks can be written with parenthesis and statements inside can be separated by comma's. If it is only one statement, it can be left out
		assertMinifier(
			"if (a == 46 && b > 4) { c = 56; } else if (ee == 5) { d = 45; e = 45} else { e = 45; d = 34; }",
			"a==46&&b>4?c=56:ee==5?(d=45,e=45):(e=45,d=34);"
		);
		// make sure we favor the '>' operator
		assertMinifier(
			"var a = 12 < b, b = b < 12, c = 12 <= b, d = b <= 12, e = 12 >= b, f = b >= 12, g = 12 > b, h = b > 12;",
			"var a=12<b,b=b<12,c=12<=b,d=b<=12,e=12>=b,f=b>=12,g=12>b,h=b>12;"
		);
		// make sure we parse the unary and binary truthy statement correctly (e.g. below is `(a) && (b < 4)` instead of `(a && b) < (4)`)
		// this is important because we rewrite `a < b` into `b > a`
		assertMinifier(
			"if (a && b < 4) { c = 56; }",
			"a&&b<4&&(c=56);"
		);
		// we put literals in comparisions on the left
		assertMinifier(
			"var a = b == null, b = b == true, c = b == false, d = b == \"str\", e = b == \"\", f = b == 0xff; var a0 = null == b, b0 = true == b, c0 = false == b, d0 = \"str\" == b, e0 = \"\" == b, f0 = 0xff == b;",
			"var a=b==null,b=b==!0,c=b==!1,d=b==\"str\",e=b==\"\",f=b==255,a0=null==b,b0=!0==b,c0=!1==b,d0=\"str\"==b,e0=\"\"==b,f0=255==b;"
		);
		assertMinifier(
			"function handleTouch(event){event = 4; return 5;}",
			"function handleTouch(e){return e=4,5}"
		);
		assertMinifier(
			"function handleTouch(event) { if (event) return 7; }",
			"function handleTouch(e){if(e)return 7}"
		);
		assertMinifier(
			"function handleTouch(event) { if (event.c) d = 4; if (event) return 7; else return 8; };",
			"function handleTouch(e){return e.c&&(d=4),e?7:8}"
		);
		assertMinifier(
			"function handleTouch(event) { if (bla.c) return 4; bla = 4; }",
			"function handleTouch(){if(bla.c)return 4;bla=4}"
		);
		assertMinifier(
			"function handleTouch(event) { if (bla.b) bla = 8; if (bla.c) return 4; bla = 4; }",
			"function handleTouch(){if(bla.b&&(bla=8),bla.c)return 4;bla=4}"
		);
		// empty return statements makes various compressing methods invalid
		assertMinifier(
			"function handleTouch(event) { if (p == 65) { if (e) { return; } b = 56; } return 6; }",
			"function handleTouch(){if(p==65){if(e)return;b=56}return 6}"
		);
		// no need for brackets in switch statement case clauses
		assertMinifier(
			"switch(a) { case b: d = 4; break; case 7: case d: d= 6; d = 19; case 7: { d= 6; } case 8: { d= 6; e = g;} break; default: d = 5; }",
			"switch(a){case b:d=4;break;case 7:case d:d=6;d=19;case 7:d=6;case 8:d=6;e=g;break;default:d=5}"
		);
		assertMinifier(
			"function def() { switch(a) { case b: d = 4; break; case 7: case d: if (g) { if (t) return; if (g) break; } case 7: { d= 6; } default: d = 5; case 8: { d= 6; e = g;} break; } }",
			"function def(){switch(a){case b:d=4;break;case 7:case d:if(g){if(t)return;if(g)break}case 7:d=6;default:d=5;case 8:d=6;e=g}}"
		);
		// returns in a switch statement cannot be moved to the first statement
		assertMinifier(
			"function getCompositionEventType(topLevelType) { switch (topLevelType) { case topLevelTypes.topCompositionStart: return eventTypes.compositionStart; case topLevelTypes.topCompositionEnd: return eventTypes.compositionEnd; case topLevelTypes.topCompositionUpdate: return eventTypes.compositionUpdate; } }",
			"function getCompositionEventType(e){switch(e){case topLevelTypes.topCompositionStart:return eventTypes.compositionStart;case topLevelTypes.topCompositionEnd:return eventTypes.compositionEnd;case topLevelTypes.topCompositionUpdate:return eventTypes.compositionUpdate}}"
		);
		assertMinifier(
			"while(true) { bla = 4; };",
			"while(!0)bla=4;"
		);
		assertMinifier(
			"function bla() { if (b) if (c) return 4; return 7}",
			"function bla(){return b&&c?4:7}"
		);
		assertMinifier(
			"do { b = 5; } while(d);",
			"do b=5;while(d);"
		);
		assertMinifier(
			"do b = 5; while(d);",
			"do b=5;while(d);"
		);
		assertMinifier(
			"function bla() { do { b = 5; } while(d); }",
			"function bla(){do b=5;while(d)}"
		);
		assertMinifier(
			"function bla() { do b = 5; while(d); }",
			"function bla(){do b=5;while(d)}"
		);
		assertMinifier(
			"function bla() { do b = 5; while(d); g = 7; };",
			"function bla(){do b=5;while(d);g=7}"
		);
		assertMinifier(
			"fun(a,b);",
			"fun(a,b);"
		);
		assertMinifier(
			"fun(a(),b);",
			"fun(a(),b);"
		);
		assertMinifier(
			"function bla() { switch(a){ case 7: if(c) return 7; return 5; }; }",
			"function bla(){switch(a){case 7:return c?7:5}}"
		);
		assertMinifier(
			"function bla(b,a) { if (b) { return null; } switch (a) { case 5: return null; default: d = 6; } };",
			"function bla(e,a){if(e)return null;switch(a){case 5:return null;default:d=6}}"
		);
		assertMinifier(
			"'use strict'; var BeforeInputEventPlugin = { eventTypes: 6 }; d = 5; g = 66;",
			"\"use strict\";var BeforeInputEventPlugin;BeforeInputEventPlugin={eventTypes:6},d=5,g=66;"
		);
		assertMinifier(
			"var BeforeInputEventPlugin = { eventTypes: 6 }; 'use strict'; d = 5; g = 66;",
			"var BeforeInputEventPlugin={eventTypes:6};d=5,g=66;"
		);
		assertMinifier(
			"if (a) for (var d = g; d < 0; d++) bla();",
			"var d;if(a)for(d=g;d<0;d++)bla();"
		);
		assertMinifier(
			"a = '0.13.3';d = a;",
			"a='0.13.3',d=a;"
		);
		// Note: if(a||b)e=5; is shorter
		assertMinifier(
			"if (a || b) e = 5;",
			"(a||b)&&(e=5);"
		);
		// Note: if(a||b)if(c&&d)e=5; is shorter
		assertMinifier(
			"if (a || b) if (c && d) e = 5;",
			"(a||b)&&c&&d&&(e=5);"
		);
		// Note: if(a||b)if(c||d)e=5; is shorter
		assertMinifier(
			"if (a || b) if (c || d) e = 5;",
			"(a||b)&&(c||d)&&(e=5);"
		);
		// Note: `if(a||b)if(c&&d||e)e=5;` is actually 2 chars shorter
		assertMinifier(
			"if (a || b) if (c && d || e) e = 5;",
			"(a||b)&&(c&&d||e)&&(e=5);"
		);
		assertMinifier(
			"if (a || b) if (c && (d || e)) e = 5;",
			"(a||b)&&c&&(d||e)&&(e=5);"
		);
		assertMinifier(
			"function a() { if (a) if (c) return bla; if (b) if (d) return alb; }",
			"function a(){return a&&c?bla:b&&d?alb:void 0}"
		);
		assertMinifier(
			"function handly(event) { if (a) { return; } b = 4; d = 6;};",
			"function handly(){!a&&(b=4,d=6)}"
		);
		assertMinifier(
			"function handly(event) { if (a) return; b = 4; d = 6;}",
			"function handly(){!a&&(b=4,d=6)}"
		);
		assertMinifier(
			"function handly(event) { if (a) return; b = 4; if (a) return; d = 6;}",
			"function handly(){!a&&(b=4,!a&&(d=6))}"
		);
		assertMinifier(
			"function handly(event) { if (!a) return; b = 4;}",
			"function handly(){a&&(b=4)}"
		);
		assertMinifier(
			"function handly(event) { if (!!a) return; b = 4;};",
			"function handly(){!a&&(b=4)}"
		);
		assertMinifier(
			"function handly(event) { if (!(a && b)) return; b = 4;}",
			"function handly(){a&&b&&(b=4)}"
		);
		assertMinifier(
			"function handly(event) { if (!(a && b)) b = 4;}",
			"function handly(){!(a&&b)&&(b=4)}"
		);
		assertMinifier(
			"function handly(event) { if ((a && b)) return; b = 4;}",
			"function handly(){!(a&&b)&&(b=4)}"
		);
		assertMinifier(
			"function handly(event) { if (c) { if ((a && b)) return; b = 4;}}",
			"function handly(){if(c){if(a&&b)return;b=4}}"
		);
		assertMinifier(
			"function handly(event) { if (c) { if ((a && b)) return; b = 4;} d = 5;}",
			"function handly(){if(c){if(a&&b)return;b=4}d=5}"
		);
		assertMinifier(
			"function bla() { if (b) if (c) return 4; else return 7}",
			"function bla(){if(b)return c?4:7}"
		);
		assertMinifier(
			"function bla() { if (b) { if (c) return 4; return 7}};",
			"function bla(){if(b)return c?4:7}"
		);
		assertMinifier(
			"function bla() { if (b) if (c) return 4; else return 7; if (d) if (g) return 6; else return 5;}",
			"function bla(){return b?c?4:7:d?g?6:5:void 0}"
		);
		assertMinifier(
			"function bla() { if (b) { if (c && bla in key) { return null; } d = 5; return chars; } }",
			"function bla(){if(b)return c&&bla in key?null:(d=5,chars)}"
		);
		assertMinifier(
			"function bla() { if (b) { return null; } if (c) return 4;}",
			"function bla(){return b?null:c?4:void 0}"
		);
		assertMinifier(
			"function bla() { if (b) { if (d) return 7; return null; } if (c) return 4;}",
			"function bla(){return b?d?7:null:c?4:void 0}"
		);
		assertMinifier(
			"function cc() { if (a) { if (!b) { return activeElementID; } } };",
			"function cc(){if(a&&!b)return activeElementID}"
		);
		assertMinifier(
			"function e(a, t) { if (t) { if ( a === t ) { return \"b\"; } return null; } switch (a) { case 7: return a ? null : t.data; } return 4; }",
			"function e(e,a){if(a)return e===a?\"b\":null;switch(e){case 7:return e?null:a.data}return 4}"
		);
		assertMinifier(
			"function e(a, t) { if (t) { if ( a === t ) { return \"b\"; } return null; } switch (a) { case 7: if (t) return null; default: return 4; } }",
			"function e(a,e){if(e)return a===e?\"b\":null;switch(a){case 7:if(e)return null;default:return 4}}"
		);
		assertMinifier(
			"function hop(e) { if (e) { for(var i=0;i<10;i++) fun(); if (targetID) { return event; } } }",
			"function hop(a){var e;if(a){for(e=0;e<10;e++)fun();if(targetID)return event}}"
		);
		assertMinifier(
			"for (var a in b) { if (!a) { continue; } };",
			"var a;for(a in b)!a;"
		);
		assertMinifier(
			"if (g) for (var a in b) g = 5;",
			"var a;if(g)for(a in b)g=5;"
		);
		assertMinifier(
			"if (g) for (var a in b) g = 5; else dd = 5;",
			"var a;if(g)for(a in b)g=5;else dd=5;"
		);
		assertMinifier(
			`a = 4; if (g) for (var i = 0; i < 10; i++) { k = 5; }`,
			"var i;if(a=4,g)for(i=0;i<10;i++)k=5;"
		);
		assertMinifier(
			`for (var i = 0; i < a.length; i++) { if (!a[i]) { d = 6; break; } }`,
			"var i;for(i=0;i<a.length;i++)if(!a[i]){d=6;break}"
		);
		assertMinifier(
			`var a = /^(?:webkit|moz|o)[A-Z]/;`,
			"var a=/^(?:webkit|moz|o)[A-Z]/;"
		);
		assertMinifier(
			`if (a=5, e) {d = 5; }`,
			"a=5,e&&(d=5);"
		);
		assertMinifier(
			`if (g) if (a=5, e) d = 5;`,
			"g&&(a=5,e)&&(d=5);"
		);
		assertMinifier(
			`if (g) if (a=5, e) d = 5; else t = 6;`,
			"g&&(a=5,e?d=5:t=6);"
		);
		assertMinifier(
			`if (g) { if (a=5, e) d = 5; b = 5;};`,
			"g&&(a=5,e&&(d=5),b=5);"
		);
		assertMinifier(
			`if (g) if (a=5, e) switch(t){ case 5: d = 3};`,
			"if(g&&(a=5,e))switch(t){case 5:d=3}"
		);
		assertMinifier(
			`switch(d = 5, t){case 5: d = 6;}`,
			"switch(d=5,t){case 5:d=6}"
		);
		assertMinifier(
			`for(a && (g = 5), d = 5; 10>a; a++) o = 5;`,
			"for(a&&(g=5),d=5;10>a;a++)o=5;"
		);
		assertMinifier(
			"for (a in k) { d = 5; if (g) k = 3; };",
			"for(a in k){d=5;g&&(k=3)}"
		);
		assertMinifier(
			`if (event.c) d = 4; if (event) switch(t){case 3: g = 4};`,
			"if(event.c&&(d=4),event)switch(t){case 3:g=4}"
		);
		assertMinifier(
			"if (a == undefined) b = 5;",
			"a==void 0&&(b=5);"
		);
		assertMinifier(
			"function a(undefined) { return undefined; }",
			"function a(e){return e}"
		);
		assertMinifier(
			`function bla() { for (;;) if (b) { if (c) return 4; } else d=5;};`,
			"function bla(){for(;;)if(b){if(c)return 4}else d=5}"
		);
		assertMinifier(
			`function bla() { for (;;) if (b) { if (c) return 4; else g = 4; } else d=5;};`,
			"function bla(){for(;;)if(b){if(c)return 4;g=4}else d=5}"
		);
		assertMinifier(
			`function d() { if (a) { if (!b) { b = 55; } else if (t) { k = 6; } } }`,
			"function d(){a&&(!b?b=55:t&&(k=6))}"
		);
		assertMinifier(
			`funcCallNeedSemicolons()`,
			"funcCallNeedSemicolons();"
		);
		assertMinifier(
			`var a=0x445;`,
			"var a=1093;"
		);
		assertMinifier(
			`var a = "asdf"; doSomething(); var b = "abadf";`,
			`var a="asdf",b="abadf";doSomething();`
		);
		assertMinifier(
			`try { throw "adsf"; } catch (e) { } finally {}`,
			`try{throw"adsf"}catch(e){}finally{}`
		);
		/// Fix #33 - return isn't emitted in nested function
		assertMinifier(
			`function a() { return function() { return 7; }}`,
			"function a(){return function(){return 7}}"
		);
		assertMinifier(
			`var a = 44; function b() { if (this.d) { this.doda(); } this.state = 77; if (this.t) this.bla(); this.state = a * 12;}`,
			`function b(){var e=this;e.d&&e.doda();e.state=77;e.t&&e.bla();e.state=a*12}var a=44;`
		);
		assertMinifier(
			`function encaps() {var no = 6; function bla() { if ("someLongString" == ops()) doOne(); if ("someLongString" == dop()) doTwo(); ko = "someLongString"; return no; };}`,
			`function encaps(){function a(){var a="someLongString";return a==ops()&&doOne(),a==dop()&&doTwo(),ko=a,e}var e=6}`
		);
		assertMinifier(
			`g = 6;var a = 6;var b = null;var c = 8`,
			"var a=6,b=null,c=8;g=6;"
		);
		/// Issue #39
		assertMinifier(
			`function n() { var longName = 44; var obj = {longName:33} return obj;}`,
			"function n(){var a=44,e={longName:33};return e}"
		);
		/// Issue #41
		assertMinifier(
			`function asdf() { if (ops.call(propName)) { var attributeName = ops[propName]; } }`,
			`function asdf(){var e;ops.call(propName)&&(e=ops[propName])}`
		);
		assertMinifier(
			`function asf(domPropertyConfig) { for (var p in Properties) { if (n.has(p)) { var a = n[p]; d.getPossibleStandardName[a] = p; d.an[p] = a; } else { d.an[p] = lowerCased; } if (dm.has(p)) { d.mm[p] = dm[p]; } else { d.mm[p] = null; } var propConfig = Properties[p]; } }`,
			`function asf(){var e,a,t;for(e in Properties){d.an[e]=n.has(e)?(a=n[e],d.getPossibleStandardName[a]=e,a):lowerCased;d.mm[e]=dm.has(e)?dm[e]:null;t=Properties[e]}}`
		);
		assertMinifier(`var a = new Error;`,`var a=new Error;`);
		/// Issue #44
		assertMinifier(
			`function e() { if (!b) return 7; if (c) return 6; throw 5; }`,
			`function e(){if(!b)return 7;if(c)return 6;throw 5;return void 0}`
		);
		/// Issue #50
		assertMinifier(
			`function e() { if (!b) return 7; else if (c) return 6; else { throw 5; return void 0 } }`,
			`function e(){if(!b)return 7;if(c)return 6;throw 5;return void 0}`
		);
		/// ditto
		assertMinifier(
			`function e() { if (!b) if (c) return 7; else if (c) return 6; else { throw 5; return void 0 } }`,
			`function e(){if(!b)if(c)return 7;else if(c)return 6;else{throw 5;return void 0}}`
		);
		/// Issue #48
		assertMinifier(
			`var a = '\'"', b = "\"'", c = '\'"' + "\"'", d = "'\"" + '\'"';`,
			`var a='\'"',b="\"'",c="'\"\"'",d="'\"'\"";`
		);
		/// Issue #51
		assertMinifier(
			`'use strict'; var CSSPropertyOperations = function(styles) { var a = ''; for (var b in styles) { var d = styles[a]; } }`,
			`"use strict";var CSSPropertyOperations;CSSPropertyOperations=function(e){var a='',t,n;for(t in e)n=e[a]};`
		);
		/// Issue #52
		assertMinifier(
			`function abc() { { var updatedIndex = update.fromIndex; var updatedChild = update.parentNode.childNodes[updatedIndex]; var parentID = update.parentID; } }`,
			`function abc(){var e,a,t;{e=update.fromIndex;a=update.parentNode.childNodes[e];t=update.parentID}}`
		);
		/// Issue #53
		assertMinifier(
			`if (Array.isArray(dL)) { for (var i = 0; i < dL.length; i++) { if (e.isPropagationStopped()) { break; } cb(e, dL[i], dispatchIDs[i]); } } else if (dL) { cb(e, dL, dispatchIDs); }`,
			`var i;if(Array.isArray(dL))for(i=0;i<dL.length&&!e.isPropagationStopped();i++)cb(e,dL[i],dispatchIDs[i]);else dL&&cb(e,dL,dispatchIDs);`
		);
		/// if-else semicolon delimiters
		assertMinifier(
			`if (d) { a = 6; } else { for (var i in x) { b[i] = ''; } }`,
			"var i;if(d)a=6;else for(i in x)b[i]='';"
		);
		/// ditto
		assertMinifier(
			`function da() { if(k) for(c=0;a.length>c;c++) { d = 6; } else  if(p) return b; return null; }`,
			"function da(){if(k)for(c=0;a.length>c;c++)d=6;else if(p)return b;return null}"
		);
		/// Issue #54 and #55
		assertMinifier(
			`function ab() { try { a(); } finally { d = 6; } return g; }`,
			"function ab(){try{a()}finally{d=6}return g}"
		);
		/// Issue #57
		assertMinifier(
			`function ab() { try { a(); } catch(df) { d = 6; } return g; }`,
			"function ab(){try{a()}catch(df){d=6}return g}"
		);
		/// Issue #62
		assertMinifier(
			`var k = a instanceof b;`,
			"var k=a instanceof b;"
		);
		/// Issue #64
		assertMinifier(
			`function isNode(d) { switch (typeof d) { case 'object': if (Array.isArray(d)) { return d.every(isNode); } for (var k in d) { if (!isNode(d[k])) { return false; } } return true; default: return false; } }`,
			`function isNode(e){var a;switch(typeof e){case 'object':if(Array.isArray(e))return e.every(isNode);for(a in e)if(!isNode(e[a]))return !1;return !0;default:return !1}}`
		);
		/// Issue #68
		assertMinifier(
			`var a = function () { return 7 }, b = function doo() { return 88; }`,
			"var a=function(){return 7},b=function doo(){return 88};"
		);
		/// Issue #50
		assertMinifier(
			`function s(o,u){ if(!n[o]){ if(!t[o]){ var a=c; if(!u&&a)return a; if(i)return i; throw f.code="MODULE_NOT_FOUND",f } t[o][0].call(l.exports,function(e){ var n=t[o][1][e];return s(n?n:e) },l,l.exports,e,t,n,r) } return n[o].exports }`,
			`function s(a,u){var d="exports",o;if(!n[a]){if(!t[a])if(o=c,!u&&o)return o;else{if(i)return i;throw f.code="MODULE_NOT_FOUND",f}t[a][0].call(l[d],function(e){var n=t[a][1][e];return s(n?n:e)},l,l[d],e,t,n,r)}return n[a][d]}`
		);
		/// backslash test
		assertMinifier(
			`var rnative = /^[^{]+\{\s*\[native \w/;`,
			`var rnative=/^[^{]+\{\s*\[native \w/;`
		);
		/// modifier test
		assertMinifier(
			`/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;`,
			`/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;`
		);
		/// Issue #93
		assertMinifier(
			`function fun() { if ( a ) { while ( b ) { if ( c ) { return true; } } } else { d = 6; } };`,
			`function fun(){if(a){while(b)if(c)return !0}else d=6}`
		);
		assertMinifier(
			`do { bla(); andBla(); } while(true);`,
			"do {bla();andBla()}while(!0);"
		);
		assertMinifier(
			`for (var k of obj) { doFun(); }`,
			`var k;for(k of obj)doFun();`
		);
		assertMinifier(
			`if (a) { switch (a) { case 5: doFun(); } } else doBla();`,
			`if(a)switch(a){case 5:doFun()}else doBla();`
		);
		assertMinifier(
			`if (a) { if (b) { switch (a) { case 5: doFun(); } } else doBla(); } else doBla();`,
			`if(a)if(b)switch(a){case 5:doFun()}else doBla();else doBla();`
		);
		assertMinifier(
			`for (;;) ;`,
			"for(;;);"
		);
		/// Issue #80
		assertMinifier(
			`Infinity;`,
			`1/0;`
		);
		/// Issue #56
		assertMinifier(
			`throw "adsf"; throw -1; throw !2; throw +4; throw (b||d)&&c; throw a; throw new Error();`,
			`throw"adsf";throw-1;throw!2;throw+4;throw(b||d)&&c;throw a;throw new Error;`
		);
		assertMinifier(
			`switch (d) { case 4: for (;;) { a = 6; bla() } }`,
			`switch(d){case 4:for(;;){a=6;bla()}}`
		);
		assertMinifier(	// this is actually ES6
			`var a = 6, b = {a};`,
			`var a=6,b={a};`
		);
		assertMinifier(
			`while (a) { if (b) { c(); break; } }`,
			`while(a)if(b){c();break}`
		);
		// other ambiguous else cases
		// for loops
		//
	}
}
/* Targets
function cc() {
  if (a)
  {
	if (!b) {
	  return activeElementID;
	}
  }
}
=>
	function cc(){if(a&&!b)return activeElementID}

function cc() {
  if (a)
  {
d = 5;
if(c)
e=4;
	if (!b) {
	  return activeElementID;
	}
  }
}
=>
	function cc(){if(a&&(d=5,c&&(e=4),!b))return activeElementID}

function cc() {
  if (a)
  {
d = 5;
if(c)
e=4;
	if (!b) {
	  return activeElementID;
	}
t=5;
  }
}
=>
	function cc(){if(a){if(d=5,c&&(e=4),!b)return activeElementID;t=5}}

function cc() {
  if (a)
  {

	if (!b) {
d=5;
	  return activeElementID;
	}
  }
}
=>
	function cc(){if(a&&!b)return(d=5,activeElementID);}


a = 5;
if (e) {d = 5; switch(t) { case 5: d = 6; } }
=>
if(a=5,e)switch(d=5,t){case 5:d=6}


a = 5;
if (e) {d = 5; for (;a<10;a++) o = 5; }
=>
if(a=5,e)for(d=5;10>a;a++)o=5;


a = 5;
if (e) {d = 5; for (a in key) o = 5; }
=>
if(a=5,e){d=5;for(a in key)o=5}


if (g)
{
a=5;
if (e) {d = 5; switch(t) { case 5: d = 6; } }
}
=>
if(g&&(a=5,e))switch(d=5,t){case 5:d=6}

*/