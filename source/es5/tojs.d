module es5.tojs;

import es5.node;
import es5.emit;
/++
	This renders the Nodes into plain JS with some simple whitespace formatting.
	Useful for unittests where we want to check the JS.
++/
auto toJS(Node root)
{
	import std.array : appender;
	auto app = appender!string;
	toJSSink(root, app);
	return app.data;
}
/// Ditto
void toJSSink(Output)(Node node, Output o)
{
	EmitState parseToCode(Node node, EmitState state = EmitState(Emit.Normal))
	{
		EmitState parseChildren(alias Open = "", alias Delimiter = "", alias Close = "", alias Spacer = " ")(Node[] children, EmitState state = EmitState(Emit.Normal))
		{
			if (children.length == 0)
			{
				o.put(Open);
				o.put(Close);
				return EmitState(Emit.Normal);
			}
			o.put(Open);
			static if (Open.length > 0)
				o.put(Spacer);
			foreach(idx,child; children[0..$-1])
			{
				auto s = parseToCode(child,state);
				if (s & Emit.RequireSemicolonDelimiter)
				{
					if (!children[idx+1].isA!"ES6.Semicolon")
						o.put("; ");
					continue;
				}
				if (s & Emit.Semicolon)
				{
					static if (Delimiter.length > 0)
						o.put(Spacer);
					continue;
				}
				o.put(Delimiter);
			}
			auto last = children[$-1];
			auto s = parseToCode(last,state);
			static if (Close.length > 0)
				o.put(Spacer);
			o.put(Close);
			return s;
		}
		EmitState r = Emit.Normal;
		import std.algorithm : canFind;
		if (node.isA!"ES6.EmptyStatement")
		{
			o.put(";");
			return EmitState(Emit.Semicolon);
		}
		if (node.name.canFind("ES6.CurlyBrackets"))
		{
			r = parseChildren!("{","; ","}")(node.children,state);
			return r;
		}
		if (node.name.canFind("ES6.Parentheses"))
		{
			r = parseChildren!("(","",")","")(node.children,state);
			return r;
		}
		if (node.name.canFind("ES6.SquareBrackets"))
		{
			r = parseChildren!("[","","]")(node.children,state);
			return r;
		}
		switch(node.name)
		{
			case "ES6.ModuleBody":
			{
				auto f = parseChildren(node.children,state);
				if (f & Emit.RequireSemicolonDelimiter)
					o.put(";");
				return EmitState(Emit.Normal);
			}
			case "ES6.AssignmentOperator":
			{
				o.put(" = ");
				return r;
			}
			case "ES6.FunctionDeclaration":
			case "ES6.FunctionDeclarationYield":
			case "ES6.FunctionDeclarationDefault":
			case "ES6.FunctionDeclarationYieldDefault":
			{
				o.put("function ");
				foreach(child; node.children[0..2])
					parseToCode(child);
				o.put(" ");
				parseToCode(node.children[2]);
				return EmitState(Emit.NoDelimiter);
			}
			case "ES6.VariableStatement":
			{
				o.put("var ");
				return parseToCode(node.children[0],state);
			}
			case "ES6.VariableDeclarationList":
			case "ES6.VariableDeclarationListIn":
			{
				assert(node.children.length > 0,"VariableDeclarationList must have children");
				foreach(p; node.children[0..$-1])
				{
					parseToCode(p,state);
					o.put(", ");
				}
				parseToCode(node.children[$-1],state);
				return EmitState(Emit.RequireSemicolonDelimiter);
			}
			case "ES6.ExpressionOperator":
			case "ES6.ExpressionOperatorIn":
			{
				if (node.matches[0] == "in")
					o.put(" in ");
				else
				{
					o.put(" ");
					o.put(node.matches[0]);
					o.put(" ");
				}
			} break;
			/*case "ES6.FromClause":
			{
				o.put(" from ");
				return parseToCode(node.children[0],state);
			}*/
			case "ES6.DefaultClauseReturn":
			case "ES6.DefaultClause":
			{
				o.put("default: ");
				parseChildren(node.children,state);
			} break;
			/*case "ES6.ImportDeclaration":
			{
				o.put("import ");
				parseChildren(node.children,state);
				o.put(";");
			} break;*/
			case "ES6.ArgumentList":
			case "ES6.ElementList":
			case "ES6.ImportsList":
			case "ES6.FormalsList":
			{
				return parseChildren!("",", ","")(node.children,state);
			}
			/*case "ES6.SuperCall":
			{
				o.put("super");
				parseChildren(node.children,state);
			} break;
			case "ES6.ClassHeritage":
			{
				o.put(" extends ");
				parseChildren(node.children,state);
			} break;
			case "ES6.ClassDeclaration":
			{
				o.put("class ");
				parseChildren(node.children,state);
			} break;*/
			/*case "ES6.FunctionDeclaration":
			{
				if (scopes.length > 1)
					return EmitState(EmitState.Normal);
			} goto case "ES6.FunctionExpression";*/
			//case "ES6.MethodDefinition":
			case "ES6.FunctionExpression":
			{
				o.put("function ");
				r = parseChildren(node.children,state);
			} break;
			case "ES6.FormalParameterList":
			case "ES6.FormalParameterListYield":
			{
				return parseChildren!("",", ","")(node.children,state);
			}
			/*case "ES6.FormalParameter":
			{
				o.put(node.matches[0]);
			} break;*/
			case "ES6.PrefixExpression":
			{
				o.put(node.matches[0]);
				if (node.matches[0][0] == 't' || node.matches[0][0] == 'd' || node.matches[0][0] == 'v') // typeof or delete or void
					o.put(" ");
			} break;
			case "ES6.ReturnStatement":
			{
				if (node.children.length == 0)
				{
					o.put("return");
					return EmitState(Emit.Normal);
				}
				o.put("return ");
				return parseChildren(node.children,state);
			}
			case "ES6.PropertyDefinitionList":
			case "ES6.BindingList":
			{
				return parseChildren!("",", ","")(node.children,state);
			}
			case "ES6.PropertyDefinition":
			{
				if (node.children[0].isA!"ES6.PropertyName")
				{
					parseToCode(node.children[0],state);
					o.put(": ");
					parseToCode(node.children[1],state);
				} else
					return parseToCode(node.children[0],state);
			} break;
			case "ES6.BooleanLiteral":
			{
				o.put(node.matches[0]);
			} break;
			case "ES6.StatementListItem":
			case "ES6.StatementListItemReturn":
			{
				r = parseToCode(node.children[0],state);
			} break;
			case "ES6.ModuleItemList":
			case "ES6.StatementList":
			case "ES6.StatementListReturn":
			{
				return parseChildren!("","; ","")(node.children,state);
			}
			case "ES6.SwitchStatement":
			case "ES6.SwitchStatementReturn":
			{
				o.put("switch ");
				parseChildren(node.children[0..1],state);
				o.put(" ");
				return parseChildren(node.children[1..$],state);
			}
			case "ES6.CaseClause":
			case "ES6.CaseClauseReturn":
			{
				o.put("case ");
				parseToCode(node.children[0],state);
				o.put(": ");
				if (node.children.length == 1)
					return r;
				parseToCode(node.children[1],state);
				o.put(";");
				return r;
			}
			case "ES6.FieldAccessor":
			{
				o.put(".");
				return parseToCode(node.children[0],state);
			}
			case "ES6.IfStatement":
			case "ES6.IfStatementYield":
			case "ES6.IfStatementReturn":
			case "ES6.IfStatementYieldReturn":
			{
				if (node.children.length > 2)
				{
					auto statementList = node.parent.parent.parent;
					o.put("if (");
					parseToCode(node.children[0].children[0],state);
					o.put(") ");
					if (node.children[1].matches[0] == "{")
					{
						parseToCode(node.children[1],state);
						//parseStatementList!("{","}",";")(node.children[1].getNthChild(4),false,state);
					}
					else
					{
						if (!(parseToCode(node.children[1],state) & Emit.Semicolon))
							o.put(";");
					}
					o.put(" else ");
					if (node.children[3].matches[0] == "{")
					{
						parseToCode(node.children[3],state);
						//parseStatementList!("{","}",";")(node.children[3].getNthChild(4),false,state);
					}
					else
					{
						o.put(" ");
						parseToCode(node.children[3],state);
					}
					return EmitState(Emit.EndingStatement);
				} else
				{
					o.put("if (");
					parseToCode(node.children[0].children[0],state);
					o.put(") ");
					if (node.children[1].matches[0] == "{")
					{
						parseToCode(node.children[1],state);
						//parseStatementList!("{","}",";")(node.children[1].getNthChild(4),false,state);
					}
					else
					{
						// here we have to do the `return` when not expecting returnvalue &&
						if (!(parseToCode(node.children[1],state) & Emit.Semicolon))
							o.put(";");
						return EmitState(Emit.Semicolon);
					}
					return EmitState(Emit.EndingStatement);
				}
			}
			case "ES6.ArrowFunctionIn": case "ES6.ArrowFunction":
			{
				parseToCode(node.children[0]);
				o.put(" => ");
				parseToCode(node.children[1]);
				break;
			}
			/*case "ES6.ArrowParameters":
			case "ES6.ArrowParametersYield":
			{
				r = parseChildren(node.children,state);
				o.put("=>");
			} break;*/
			case "ES6.Initializer":
			case "ES6.InitializerIn":
			case "ES6.InitializerYield":
			case "ES6.InitializerInYield":
			{
				o.put(" = ");
				return parseToCode(node.children[0],state);
			}
			case "ES6.LetOrConst":
			{
				o.put(node.matches[0]);
				o.put(" ");
			} break;
			case "ES6.InKeyword":
				o.put(" in ");
				break;
			case "ES6.NewKeyword":
				o.put("new ");
				break;
			case "ES6.OfKeyword":
				o.put(" of ");
				break;
			case "ES6.VarKeyword":
				//if (!node.parent.parent.isA!"ES6.IterationStatement")
					o.put("var ");
				break;
			case "ES6.ConditionalExpression":
			case "ES6.ConditionalExpressionIn":
			case "ES6.ConditionalExpressionYield":
			case "ES6.ConditionalExpressionInYield":
			{
				parseToCode(node.children[0]);
				if (node.children.length > 1)
				{
					o.put(" ? ");
					parseToCode(node.children[1]);
					o.put(" : ");
					parseToCode(node.children[2]);
				}
			} break;
			case "ES6.IterationStatement":
			case "ES6.IterationStatementYield":
			case "ES6.IterationStatementReturn":
			case "ES6.IterationStatementYieldReturn":
			{
				auto iter = node.asIterationStatement;

				if (!iter.isDoWhileIteration)
					goto default;
				o.put("do ");

				if (iter.hasBlockStatement)
				{
					auto list = iter.getStatementList();
					if (list.children.length > 1)
						o.put("{ ");
					parseToCode(list,state);
					if (list.children.length > 1)
						o.put(" }");
					else
						o.put(";");
				} else
				{
					parseToCode(iter.getStatement,state);
					o.put(";");
				}
				o.put("while ");
				parseToCode(node.children[3],state);
				o.put(";");
				return EmitState(Emit.Semicolon);
			}
			case "ES6.RegularExpressionLiteral":
			{
				o.put('/');
				parseToCode(node.children[0]);
				o.put('/');
				if (node.children.length == 2)
					parseToCode(node.children[1]);
				return r;
			}
			case "ES6.RegularExpressionClass":
			{
				o.put('[');
				parseToCode(node.children[0]);
				o.put(']');
				return r;
			}
			case "ES6.RegularExpressionBackslashSequence":
			{
				o.put("\\");
				return parseToCode(node.children[0]);
			}
			case "ES6.TryStatement":
			case "ES6.TryStatementReturn":
			{
				o.put("try ");
				goto default;
			}
			case "ES6.Catch":
			case "ES6.CatchReturn":
			{
				o.put(" catch ");
				parseToCode(node.children[0]);
				o.put(" ");
				parseToCode(node.children[1]);
				return r;
			}
			case "ES6.ThrowStatement":
			{
				o.put("throw ");
				parseToCode(node.children[0]);
				o.put(";");
				return EmitState(Emit.Semicolon);
			}
			case "ES6.Finally":
			case "ES6.FinallyReturn":
			{
				o.put(" finally ");
				return parseToCode(node.children[0]);
			}
			case "ES6.LexicalDeclaration":
			case "ES6.LexicalDeclarationIn":
			{
				r = parseChildren(node.children,state);
				o.put("; ");
				return r;
			}
			case "ES6.IdentifierName":
			{
				o.put(node.matches[0]);
			} break;
			default:
				if (node.children.length == 0)
				{
					if (node.matches.length == 0)
						return EmitState(Emit.Skipped);
					foreach(m; node.matches)
						o.put(m);
				}
				else
					return parseChildren(node.children,state);
		}
		return r;
	}
	parseToCode(node);
}
version (unittest)
{
	import unit_threaded;
	import es5.testhelpers;
	unittest
	{
		import es6.grammar;
		import std.stdio;
		void assertEqual(string input, string expected, in string file = __FILE__, in size_t line = __LINE__)
		{
			auto p = ES6(input);
			auto nodes = createNode(p);
			auto got = nodes.toJS();
			if (got == expected)
				return;
			Message m;
			m.writeln(nodes);
			m.writeln("Got:");
			m.writeln(got);
			m.writeln("Expected:");
			m.writeln(expected);
			failed(m,file,line);
		}
		assertEqual(`if (d) a = 4`,`if (d) a = 4;`);
		assertEqual(`if (d) { a = 4 }`,`if (d) { a = 4 }`);
		assertEqual(`var a = 7 < 4 ? true : false;`,`var a = 7 < 4 ? true : false;`);
		assertEqual(`var a = /^(?:webkit|moz|o)[A-Z]/;`,`var a = /^(?:webkit|moz|o)[A-Z]/;`);
		assertEqual(`try { throw "adsf"; } catch (e) {} finally {}`,`try { throw "adsf"; } catch (e) {} finally {}`);
		assertEqual(`var a = new Error();`,`var a = new Error();`);
		assertEqual(`for(var o=0;o<r.length;o++)s(r[o]);`,`for(var o = 0;o < r.length;o++)s(r[ o ])`);
		assertEqual(`var rnative = /^[^{]+\{\s*\[native \w/;`,`var rnative = /^[^{]+\{\s*\[native \w/;`);
		assertEqual(`var rnative = /^[^{]+\{\s*\[native \w/;`,`var rnative = /^[^{]+\{\s*\[native \w/;`);
		assertEqual(`/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g`,`/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g`);
		assertEqual(`do bla();while (true);`,`do bla();while (true);`);
		assertEqual(`do { bla(); } while (true);`,`do bla();while (true);`);
		assertEqual(`do { bla(); b = 6 }while (true);`,`do { bla(); b = 6 }while (true);`);
		assertEqual(`for(var k in b) { bla(); }`,`for(var k in b){ bla() }`);
		assertEqual(`for(var k of b) bla();`,`for(var k of b)bla()`);
		assertEqual(`function abc(a,b,c) { switch(a) { case 7: bla(!b); break; case 8: return; default: return { a: 7 } }; }`,`function abc(a, b, c) { switch (a) { case 7: bla(!b); break;case 8: return;; default: return { a: 7 } } }`);
		assertEqual(`while(true) { bla(); }`,`while(true){ bla() }`);
		assertEqual(`while(true) bla();`,`while(true)bla()`);
		assertEqual(`for(a && 6;a < 6;a++);`,`for(a && 6;a < 6;a++);`);
		assertEqual(`var a = { abc };`,`var a = { abc };`);
		assertEqual(`66`,`77`).shouldThrow();
		assertEqual(`var odds = evens.map(v => v + 1);`,`var odds = evens.map(v => v + 1);`);
		assertEqual(`for (let c = 6, d; a; c++) { bla = 5; }`,`for(let c = 6, d; a;c++){ bla = 5 }`);
		assertEqual(`for (const c = 6, d; a; c++) { bla = 5; }`,`for(const c = 6, d; a;c++){ bla = 5 }`);
	}
}