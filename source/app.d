version (MinifyApp):

import es6.grammar;
import es5.node;
import es5.minifier;
import std.getopt;
import es5.analyse;
import es5.optimize;

int main(string[] args)
{
	import std.stdio : stdin;
	import std.file : readText, write, exists;
	import std.algorithm : joiner, map;
	import std.stdio : writeln;
	import std.conv : text, to;
	import std.algorithm : remove;
	import std.path : baseName;
	bool time;
	bool forceStdin;
	string fileIn;
	string fileOut;

	auto helpInformation = getopt(
		args,
		"time", "Show timing information", &time,
		"i|input", "Input file (defaults to stdin)", &fileIn,
		"o|output", "Output file (defaults to stdout)", &fileOut
	);

	if (helpInformation.helpWanted)
	{
		defaultGetoptPrinter("ECMAScript 5 minifier.\n\nUsage:\t"~baseName(args[0])~" [OPTIONS]\n\nOptions:\n", helpInformation.options);
		return 1;
	}

	import std.datetime : StopWatch;
	StopWatch sw;
	string[] timing;
	ParseTree p;
	sw.start();
	if (fileIn.length > 0)
	{
		if (!exists(fileIn))
		{
			writeln("File `",fileIn,"` doesn't exist.");
			assert(false);
		}
		p = ES6(readText(fileIn));
	}
	else
	{
		auto t = stdin.byChunk(4096).map!(c=>cast(char[])c).joiner.to!string;
		p = ES6(t);
	}
	if (!p.successful)
	{
		writeln("Couldn't parse...");
		writeln(p);
		assert(false);
	}
	sw.stop();
	if (time)
		writeln("Parsing: "~sw.peek().msecs.to!string~"ms");
	sw.reset();

	sw.start();
	auto nodes = createNode(p);
	sw.stop();
	if (time)
		writeln("Nodes: "~sw.peek().msecs.to!string~"ms");
	sw.reset();

	if (!nodes.successful)
	{
		writeln("Invalid Input");
		return 1;
	}
	sw.start();
	nodes.optimize();
	moveVariableAndFunctionDeclarationsUpFront(nodes);
	sw.stop();
	if (time)
		writeln("Order vars and funcs: "~sw.peek().msecs.to!string~"ms");
	sw.reset();

	sw.start();
	auto a = analyseNodes(nodes);
	sw.stop();
	if (time)
		writeln("Analyse: "~sw.peek().msecs.to!string~"ms");
	sw.reset();

	sw.start();
	optimize(nodes,a);
	sw.stop();
	if (time)
		writeln("Optimize: "~sw.peek().msecs.to!string~"ms");
	sw.reset();

	sw.start();
	import es5.transforms.variables;
	a.scp.shortenVariables();
	sw.stop();
	if (time)
		writeln("Shorten Identifiers: "~sw.peek().msecs.to!string~"ms");
	sw.reset();

	sw.start();
	auto min = nodes.minify();
	sw.stop();
	if (time)
		writeln("Emitting: "~sw.peek().msecs.to!string~"ms");
	sw.reset();

	if (fileOut.length > 0)
		write(fileOut,min);
	else 
		writeln(min);

	return 0;
}